
<br><br>
<div class="container">
  <div class="row">

    <div style="padding:25px" class="col-md-8">
      <h1 style="color: #F02440; background-color: #0A9EED; color: white; border-radius:15px;" class="text-center">Nuestras Escuelas <br></h1>
    </div>

    <div class="col-md-4">
      <img src="<?php echo base_url(); ?>/assets/images/logo.png" width="40%" alt="">
    </div>

  </div>
</div>



<div style="padding: 22px;" class="row">
  <div class="col-md-12">
    <div id="mapaEscuelas" style="width: 100%; height: 500px; border:2px solid black;"></div>
  </div>
</div>


<script type="text/javascript">

        function initMap(){
        //estamos instanciando un punto o coordenada en el mapa
        var centro = new google.maps.LatLng(-0.9338643832584594, -78.61299035537151);

        //estamos instanciando un mapa
        var mapaEscuelas= new google.maps.Map(
            document.getElementById("mapaEscuelas"),  //aqui se pone el id del div donde se va a mostrar el mapa que se captura a traves de su ID
            {
            center: centro,
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.HYBRID
            }
        );


        var marcador = new google.maps.Marker({
            position: centro,
            title: "marcador de prueba",
            map: mapaEscuelas

            // icon: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png"
            });

        <?php if($escuelas): ?>  //con estom validas que si hay lugares en la base de datos, se muestren en el mapa
            <?php foreach($escuelas as $lugarTemporal): ?>  //con esto recorres los lugares que hay en la base de datos

            //construimos otra coordenada
            var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_esc; ?>, <?php echo $lugarTemporal->longitud_esc; ?>);

            var marcador = new google.maps.Marker({
            position: coordenadaTemporal,
            title: "<?php echo $lugarTemporal->barrio_esc; ?> - <?php echo $lugarTemporal->ciudad_esc ?> - <?php echo $lugarTemporal->telefono_esc; ?>",
            map: mapaEscuelas,

            icon: "<?php echo base_url(); ?>/assets/images/escuela.png"
            });

            <?php endforeach; ?>

        <?php endif; ?>
}

</script>
