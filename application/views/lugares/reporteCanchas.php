
<br><br>
<div class="container">
  <div style="padding:20px" class="row">

    <div class="col-md-8">
      <h1 style="color: #F02440; background-color: #2DB7A9; color: white; border-radius:15px;" class="text-center">Canchas Bocha Sport 
      <br></h1> 
    </div>

    <div class="col-md-4">
    <img src="<?php echo base_url(); ?>/assets/images/canchas.avif" width="20%" alt="">
    </div>



  </div>
</div>



<div style="padding: 22px;" class="row">
  <div class="col-md-12">
    <div id="mapaLugaresCancha" style="width: 100%; height: 500px; border:2px solid black;"></div>
  </div>
</div>


<script type="text/javascript">

        function initMap(){
        //estamos instanciando un punto o coordenada en el mapa
        var centro = new google.maps.LatLng(-0.9338643832584594, -78.61299035537151);

        //estamos instanciando un mapa
        var mapaCanchas= new google.maps.Map(
            document.getElementById("mapaLugaresCancha"),  //aqui se pone el id del div donde se va a mostrar el mapa que se captura a traves de su ID
            {
            center: centro,
            zoom: 3,
            mapTypeId: google.maps.MapTypeId.HYBRID
            }
        );



        <?php if($cancha): ?>  //con estom validas que si hay lugares en la base de datos, se muestren en el mapa
            <?php foreach($cancha as $lugarTemporalC): ?>  //con esto recorres los lugares que hay en la base de datos

            //construimos otra coordenada
            var coordenadaTemporalC=new google.maps.LatLng(<?php echo $lugarTemporalC->latitud_can; ?>, <?php echo $lugarTemporalC->longitud_can; ?>);

            var marcador = new google.maps.Marker({
            position: coordenadaTemporalC,
            title: "<?php echo $lugarTemporalC->nombre_can; ?> <?php echo $lugarTemporalC->pais_can; ?> - <?php echo $lugarTemporalC->telefono_can; ?>",
            map: mapaCanchas,

            icon: "<?php echo base_url(); ?>/assets/images/cancha.png"
            });

            <?php endforeach; ?>

        <?php endif; ?>
}

</script>
