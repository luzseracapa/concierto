<br>
<div class="row">
  <div class="col-md-8">
    <h1>LISTADO DE LESIONES</h1>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url('lesiones/nuevo') ?>"class="btn btn-success">
      <i class="glypicon glypicon-plus"></i>
      Agregar lesion
    </a>
  </div>
</div>
<br>
<?php if ($lesiones):?>
    <table class="table table-striped table-bordered" >
        <thead>
            <tr>
                <th>ID</th>
                <th>TIPO</th>
                <th>DESCRIPCION</th>
                <th>MEDICO</th>
                <th>TRATAMIENTO</th>
                <th>TIEMPO DE RECUPERACION</th>
                <th>ESTADO</th>
                <th>OBSERVACIONES</th>
                <th>ACCIONES</th>


            </tr>
        </thead>
        <tbody>
            <?php foreach($lesiones as $filaTemporal):?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_le; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Tipo_le; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Descripcion_le; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Medico_le; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Tratamiento_le; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Tiempo_de_recuperacion_le; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Estado_le; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->observaciones_le; ?>
                    </td>

                     <td class="text-center">
                      <a href="<?php echo site_url('/lesiones/editar/'), $filaTemporal->id_le; ?> " title="editar registro">

                    <i class="bi bi-pen"></i>
                      </a>
                        &nbsp;&nbsp;&nbsp
                      <a href="<?php echo site_url('/lesiones/eliminar/'), $filaTemporal->id_le; ?> "title="eliminar registro"
                        onclick="return confirm('¿Estas Seguro?');"
                        style="color:red;">

                    <i class="bi bi-trash"></i>
                      </a>
                    </td>

                </tr>
            <?php endforeach;?>

        </tbody>

    </table>
<?php else: ?>
    <h1 style="text-align:center">No existe ninguna informacion! </h1>
<?php endif;?>
