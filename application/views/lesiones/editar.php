<h1 style="text-align:center">EDITAR REGISTRO LESION </h1>
<br>
<form class=""
id="frm_nueva_lesion"
action="<?php echo site_url();?>/lesiones/procesarActualizacion"
method="post">
<div class="row">
      <div class="col-md-4">
      <label for="">
      </label>
      <br>
      <input type="number"
      class="form-control"
      name="id_le" value="<?php echo $lesionEditar->id_le; ?>"hidden
      id="id_le">
  </div>
  <br>
  <br>
  <br>
  <br>
</div>
    <div class="row">
        <div class="col-md-4">
          <label for>Tipo de lesion:</label>
          <br>
            <div class="col-md-12">
            <select name="Tipo_le" class="form-control" readonly placeholder="Ingrese" id="Tipo_le">
                    <option value="Fractura de hueso. ">Fractura de hueso. </option>
                    <option value="Esguince">Esguince</option>
                    <option value="Tendinitis aquilea">Tendinitis aquilea</option>
                    <option value="Tendinitis del aductor.">Tendinitis del aductor.</option>
                    <option value="Osteopatía de pubis.">Osteopatía de pubis.</option>
                    <option value="Dislocación.">Dislocación.</option>
            </select>
          </div>
        </div>
      <div class="col-md-4">
        <label for="">Medico:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el medico encargado"
        class="form-control"
        name="Medico_le" value="<?php echo $lesionEditar->Medico_le; ?>"
        id="Medico_le">
      </div>
      <div class="col-md-4">
        <label for>Estado:</label>
        <br>
          <div class="col-md-12">
          <select name="Estado_le" class="form-control" readonly  placeholder="Ingrese" id="Estado_le">
                  <option value="Leve">Leve </option>
                  <option value="Moderada">Moderada</option>
                  <option value="Grave">Grave</option>
          </select>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="col-md-12">
        <label for="">Descripcion:
          <span class="obligatorio">(Obligatorio)</span>
        </label>
        <br>
        <input type="text"
        placeholder="Ingrese una descripcion"
        class="form-control"
        required
        name="Descripcion_le" value="<?php echo $lesionEditar->Descripcion_le; ?>"
        id="Descripcion_le">
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Tratamiento:
          <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el tratamiento"
          class="form-control"
          required
          name="Tratamiento_le" value="<?php echo $lesionEditar->Tratamiento_le; ?>"
          id="Tratamiento_le">
      </div>
      <div class="col-md-4">
          <label for="">Tiempo de recuperacion:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el tiempo de recuperacion"
          class="form-control"
          required
          name="Tiempo_de_recuperacion_le" value="<?php echo $lesionEditar->Tiempo_de_recuperacion_le; ?>"
          id="Tiempo_de_recuperacion_le">
      </div>
      <div class="col-md-4">
          <label for="">Observaciones:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese alguna observacion"
          class="form-control"
          required
          name="observaciones_le" value="<?php echo $lesionEditar->observaciones_le; ?>"
          id="observaciones_le">
      </div>

    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/lesiones/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
<br>
<br>
<br>

<!-- validacion con jquery validate -->
<script type="text/javascript">
  $("#frm_nueva_lesion").validate({
    rules:{
      Tipo_le:{
      //  digits:true,
      //letras:true,
         required:true,
        // minlength:10,
        // maxlength:10,
        },
      Descripcion_le:{
        letras:true,
        required:true,
        minlength:3,
        maxlength:150,
        },
        Medico_le:{
        letras:true,
        required:true,
        minlength:3,
        maxlength:150,
        },
        Tratamiento_le:{
        letras:true,
        required:true,
        minlength:3,
        maxlength:150,
        },
        Tiempo_de_recuperacion_le:{
        letras:true,
        required:true,
        minlength:10,
        maxlength:150,
        },
        Estado_le:{
        // letras:true,
        required:true,
        // minlength:5,
        // maxlength:150,
      },
      observaciones_le:{
      letras:true,
      required:true,
      minlength:5,
      maxlength:150,
      },

    },
    messages:{
      Tipo_le:{
        digits:"Solo se acepta numeros",
        number:"Solo se acepta letras",
        required:"Selecione el tipo de lesion",
        minlength:"debe tener al menos 3 caracteres",
        maxlength:"debe tener al menos 3 caracteres"
      },
      Descripcion_le:{
        letras:"Solo se aceptan letras",
        required:"Por Favor ingrese la descripcion",
        minlength:"debe tener al menos 3 caracteres",
        maxlength:"Apellido incorrecto"
      },
        Medico_le:{
        required:"Por Favor ingrese el Meido encargado",
        minlength:"debe tener al menos 3 caracteres",
        maxlength:"Nombres incorrectos"
      },
      Tratamiento_le:{
      required:"Por Favor ingrese el tratamiento",
      minlength:"debe tener al menos 3 caracteres",
      maxlength:"Titulo incorrecto"
      },
      Tiempo_de_recuperacion_le:{
        letras:"Solo se aceptan letras",
      // digits:"Solo se acepta numeros",
      // number:"Solo se acepta numeros",
      required:"Por Favor ingrese el tiempo de la recuperacion.",
      minlength:"debe tener al menos 10 caracteres",
      maxlength:" incorrecto"
      },
      Estado_le:{
      required:"Selecione el estado de la lesion."
      // minlength:"debe tener al menos 5 caracteres",
      // maxlength:"Dato  incorrecto",
    },
    observaciones_le:{
    required:"Ingrese las observaciones pertinentes",
    minlength:"debe tener al menos 5 caracteres",
    maxlength:"Dato  incorrecto"
    }
    }
  });
</script>
