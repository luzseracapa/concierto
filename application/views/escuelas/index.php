<div style="padding-left:50px; padding-top: 25px;" class="container">
    <div class="row">
        <div class="col-md-8">
            <h3>LISTADO DE EVENTOS</h3>
        </div>

        <div class="col-md-4">
            <a style="color: black;" href="<?php echo site_url('escuelas/nuevo') ?>" class="btn btn-info btn-sm"><i class="bi bi-person-plus"></i> REGISTRAR EVENTO</a>
        </div>
    </div>
</div>

<?php if($escuelas): ?>
    <div style="padding:25px" class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">BARRIO</th>
                            <th scope="col">CIUDAD</th>
                            <th>AFORO</th>
                            <th>CANTANTE</th>
                            <th>LATITUD</th>
                            <th>LONGITUD</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($escuelas as $filatemporal): ?>
                        <tr class="">
                            <td scope="row"><?php echo $filatemporal->id_esc ?></td>
                            <td><?php echo $filatemporal->barrio_esc ?></td>
                            <td><?php echo $filatemporal->ciudad_esc ?></td>
                            <td><?php echo $filatemporal->telefono_esc ?></td>
                            <td><?php echo $filatemporal->nombre_ent .' '. $filatemporal->apellido_ent ?></td>
                            <td><?php echo $filatemporal->latitud_esc ?></td>
                            <td><?php echo $filatemporal->longitud_esc ?></td>
                            <!-- Inicio botones -->
                            <td>
                                <a href="<?php echo site_url(); ?>/escuelas/editar/<?php echo $filatemporal->id_esc ?>" title="Editar Escuela" class="btn btn-warning btn-sm"><i class="bi bi-pen-fill"></i></a>

                                <!-- BOTON ELIMINAR -->
                                <a href="<?php echo site_url();?>/escuelas/eliminar/<?php echo $filatemporal->id_esc ?>"

                                onclick="return confirm('¿Está seguro de eliminar el registro?')"


                                title="Eliminar Escuela" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>

                            </td>
                            <!-- Fin botones -->
                        <?php endforeach; ?>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>

<?php else: ?>
    <h1>No hay pedidos</h1>

<?php endif; ?>
