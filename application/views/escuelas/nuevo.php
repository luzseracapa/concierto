

<div style="padding:25px" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                EVENTO
            </div>

            <div class="card-body">
                <!-- inicio del post -->
                <form id="frm_nuevo_escuela" action="<?php echo site_url(); ?>/Escuelas/guardar" method="post">

                    <!-- inicio barrio -->
                    <div class="mb-3">
                      <label for="" class="form-label">Barrio:</label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text"
                        class="form-control" name="barrio_esc" id="barrio_esc" aria-describedby="helpId" placeholder="">
                    </div>
                    <!-- fin barrio -->

                    <!-- inicio ciudad -->
                    <div class="mb-3">
                      <label for="" class="form-label">Ciudad:</label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text"
                        class="form-control" name="ciudad_esc" id="ciudad_esc" aria-describedby="helpId" placeholder="">
                    </div>
                    <!-- fin ciudad -->

                    <div class="row">
                        <!-- inicio telefono -->
                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label">AFORO:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="number"
                                class="form-control" name="telefono_esc" id="telefono_esc" aria-describedby="helpId" placeholder="">
                            </div>
                        </div>
                        <!-- fin telefono -->

                        <div class="col-md-6"></div>
                    </div>

                    <!-- inicio entrenador -->
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mb-3">
                              <label for="entrenador_id" class="form-label">CANTANTE:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <select class="form-control" name="entrenador_id" id="entrenador_id" required>
                                <option value="">Seleccione el cantante</option>
                                <?php foreach ($entrenadores as $entrenador){ ?>
                                    <option value="<?php echo $entrenador->id_ent; ?>"><?php echo $entrenador->nombre_ent . ' ' . $entrenador->apellido_ent; ?></option>

                                <?php } ?>

                              </select>
                            </div>
                        </div>
                    </div>
                    <!-- fin entrenador -->

                        <!-- inicio row para laitus y longitud -->
                        <!-- se debe colocar un id  -->
                        <!-- a nivel de base de datos el tipo de datos en longitud y latityud debe ser double -->
                        <!-- id con un numeros -- class con un punto . -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                  <label for="" class="form-label"><b>Latitud:</b></label>
                                  <span class="obligatorio">(Obligatorio)</span>
                                  <input type="text"  readonly
                                    class="form-control" name="latitud_esc" id="latitud" aria-describedby="helpId" placeholder="Selecciona la ubicación en el mapa">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="mb-3">
                                  <label for="" class="form-label"><b>Longitud:</b></label>
                                  <span class="obligatorio">(Obligatorio)</span>
                                  <input type="text" readonly
                                    class="form-control" name="longitud_esc" id="longitud" aria-describedby="helpId" placeholder="Selecciona la ubicación en el mapa">
                                </div>
                            </div>

                        </div>

                        <!-- estamos colocando el espacio para poner el mapa -->
                        <div style="padding: 22px;" class="row">
                        <div class="col-md-12">
                            <div id="mapaUbicacion" style="width: 100%; height: 500px; border:2px solid black;"></div>
                        </div>
                        </div>
                        <!-- fin de colocando espacio para poner el mapa -->
                        <!-- fin row para laitus y longitud -->

                        <!-- inicio botones -->
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                                &nbsp;
                                <a href="<?php echo site_url(); ?>/escuelas/index" class="btn btn-danger">Cancelar</a>
                            </div>
                        </div>
                        <br><br>
                        <!-- fin botones -->



                </form>
                <!-- fin del post -->
            </div>
            <div class="card-footer text-muted">
            </div>

        </div>
    </div>
</div>


<!-- estamos instanciando el mapa de ubicacion para que se registre solo tocando la ubicacion -->
<script type="text/javascript">
    function initMap(){
      var centro = new google.maps.LatLng(-1.6364025532680684, -78.65209578103213);

      var mapa1= new google.maps.Map(
        document.getElementById("mapaUbicacion"),
        {
          center: centro,
          zoom: 7,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

      );

      var marcador = new google.maps.Marker({
          position: centro,
          title: "Centro de la ciudad",
          map: mapa1,
          draggable: true,

          icon: "<?php echo base_url(); ?>assets/images/icon4.png"
        });

        google.maps.event.addListener(marcador, 'dragend', function(){ //cuando termine la arrastrada del marcador, se ejecuta la funcion

          document.getElementById("latitud").value=this.getPosition().lat();
          document.getElementById("longitud").value=this.getPosition().lng();
        });



    } //cierre de la funcion
</script>
<!-- fin de la funcion -->

<script type="text/javascript" >
  $("#frm_nuevo_escuela").validate({
    rules:{
      barrio_esc:{
        required: true,
        minlength: 3,
        maxlength: 150,
        letras: true
      },
      ciudad_esc:{
        required: true,
        minlength: 3,
        maxlength: 150,
        letras: true
      },
      telefono_esc:{
        required: true,
        minlength: 1,
        maxlength: 6
      },
      entrenador_id:{
        required: true
      },
      latitud_esc:{
        required: true
      },
      longitud_esc:{
        required: true
      }
    },
    messages:{
      barrio_esc:{
        required: "El campo es obligatorio",
        minlength: "El campo debe tener minimo 3 caracteres",
        maxlength: "El campo debe tener maximo 150 caracteres",
        letras: "El campo solo debe tener letras"
      },
      ciudad_esc:{
        required: "El campo es obligatorio",
        minlength: "El campo debe tener minimo 3 caracteres",
        maxlength: "El campo debe tener maximo 150 caracteres",
        letras: "El campo solo debe tener letras"
      },
      telefono_esc:{
        required: "El campo es obligatorio",
        minlength: "El campo debe tener minimo 1 caracteres",
        maxlength: "El campo debe tener maximo 6 caracteres",
        number: "El campo solo debe tener numeros"
      },
      entrenador_id:{
        required: "El campo es obligatorio"
      },
      latitud_esc:{
        required: "El campo es obligatorio"
      },
      longitud_esc:{
        required: "El campo es obligatorio"
      }

    }

  });

</script>
