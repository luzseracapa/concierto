<br><br>
<div  style="padding-left:20px" class="row">
    <div class="col-md-8">
        <h1>Listado de Vacantes</h1>
    </div>

    <div class="col-md-4">
        <a name="" id="" class="btn btn-primary" href=" <?php echo site_url('vacantes/nuevaVacante'); ?>" role="button"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-plus" viewBox="0 0 16 16">
  <path d="M8.5 6a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V10a.5.5 0 0 0 1 0V8.5H10a.5.5 0 0 0 0-1H8.5V6z"/>
  <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
</svg> Agregar Vacante</a>
    </div>
</div>



<?php if($vacantes): ?>
    <div style="padding:25px" class="row">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">PUESTO</th>
                        <th scope="col">SALARIO</th>
                        <th>HORAS/MENSUALES</th>
                        <th>EXPERIENCIA</th>
                        <th>ESTUDIOS</th>
                        <th>AREA</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($vacantes as $filatemporal): ?>
                    <tr class="">
                        <td scope="row"><?php echo $filatemporal->id_vac ?></td>
                        <td><?php echo $filatemporal->puesto_vac ?></td>
                        <td><?php echo $filatemporal->salario_vac ?></td>
                        <td><?php echo $filatemporal->horas_vac ?></td>
                        <td><?php echo $filatemporal->experiencia_vac ?></td>
                        <td><?php echo $filatemporal->estudios_vac ?></td>
                        <td><?php echo $filatemporal->area_vac ?></td>
                        <!-- inicio acciones -->
                        <td>
                                <a href="<?php echo site_url(); ?>/vacantes/editar/<?php echo $filatemporal->id_vac?>" title="Editar Vacante" class="btn btn-warning btn-sm"><i class="bi bi-pen-fill"></i></a>

                                <!-- BOTON ELIMINAR -->
                                <a href="<?php echo site_url();?>/vacantes/eliminar/<?php echo $filatemporal->id_vac ?>"

                                onclick="return confirm('¿Está seguro de eliminar el registro?')"


                                title="Eliminar Vacante" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>

                        </td>

                    <?php endforeach;?>
                    </tr>

                </tbody>
            </table>
        </div>

    </div>

<?php else: ?>
    <h1>No hay Vacantes</h1>


<?php endif; ?>
