

<div style="padding:25px" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Editar Vacante
            </div>
            <div class="card-body">
                <!-- inicio del form -->
                <form id="frm_nuevo_vacante" action="<?php echo site_url('Vacantes/procesarActualizacion'); ?>" method="post">
                  <!-- inicio ID -->
                      <div class="row">
                          <div class="col-md-4">
                              <div class="mb-3">
                                <label for="" class="form-label">ID:</label>
                                <input type="text" value="<?php echo $vacanteEditar->id_vac?>" readonly
                                  class="form-control" name="id_vac" id="id_ent" aria-describedby="helpId" placeholder="">
                              </div>
                          </div>
                      </div>
                      <!-- fin ID -->

                    <div class="row">
                        <!-- inicio de la puesto -->
                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label">Puesto:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="Text" value="<?php echo $vacanteEditar->puesto_vac?>"  required
                                class="form-control" name="puesto_vac" id="" aria-describedby="helpId" placeholder="Puesto vacante">
                            </div>

                        </div>
                        <!-- fin de la puesto -->

                        <div class="col-md-6"></div>
                    </div>

                    <div class="row">
                        <!-- inicio nombre -->
                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label">Salario:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="number" value="<?php echo $vacanteEditar->salario_vac?>" required
                                class="form-control" name="salario_vac" id="" aria-describedby="helpId" placeholder="Salario propuesto.">
                            </div>

                        </div>
                        <!-- fin nombre -->

                        <!-- inicio apellido -->
                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label">Horas de trabajo:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="number" value="<?php echo $vacanteEditar->horas_vac?>" required
                                class="form-control" name="horas_vac" id="" aria-describedby="helpId" placeholder="Horas mensuales">
                            </div>

                        </div>
                        <!-- fin apellido -->

                        <!-- inicio direccion -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                  <label for="" class="form-label">Experiencia:</label>
                                  <span class="obligatorio">(Obligatorio)</span>
                                  <input type="number" value="<?php echo $vacanteEditar->experiencia_vac?>"
                                    class="form-control" name="experiencia_vac" id="" aria-describedby="helpId" placeholder="Años de experiencia">
                                </div>
                            </div>
                        </div>
                        <!-- fin direccion -->

                        <div class="row">
                            <!-- inicio email -->
                            <div class="col-md-6">
                                <div class="mb-3">
                                  <label for="" class="form-label">Estudios:</label>
                                  <span class="obligatorio">(Obligatorio)</span>
                                  <input type="text" value="<?php echo $vacanteEditar->estudios_vac?>"
                                    class="form-control" name="estudios_vac" id="" aria-describedby="helpId" placeholder="Nivel de estudio requerido">
                                </div>
                            </div>
                            <!-- fin email -->

                            <div class="col-md-6">
                            <!-- inicio telefono -->
                            <div class="mb-3">
                              <label for="" class="form-label">Área:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="text" value="<?php echo $vacanteEditar->area_vac?>"
                                class="form-control" name="area_vac" id="" aria-describedby="helpId" placeholder="Area de vacante.">
                            </div>
                            <!-- fin telefono -->
                            </div>

                        </div>

                    </div>
                    <!-- inicio botones -->
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" name="button"
                            class="btn btn-primary">
                            Editar
                            </button>
                            &nbsp;
                            <a href="<?php echo site_url(); ?>/vacantes/listarVacante" class="btn btn-danger">
                            Cancelar
                            </a>
                        </div>
                    </div>
                    <!-- fin botones -->
                </form>
                <!-- fin del form -->


            </div>
            <div class="card-footer text-muted">
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" >
    $("#frm_nuevo_vacante").validate({
        rules:{
            puesto_vac:{
                letras:true,
                required:true,
                minlength:5,
                maxlength:20
            },
            salario_vac:{
                required:true,
                minlength:3,
                maxlength:10,

            },
            horas_vac:{
                required:true,
                minlength:1,
                maxlength:3,
            },
            experiencia_vac:{
                required:true,
                minlength:1,
                maxlength:3,
            },
            estudios_vac:{
                required:true,
                letras:true,
                minlength:5,
                maxlength:15
            },
            area_vac:{
                required:true,
                letras:true,
                minlength:5,
                maxlength:15
            },
        },messages:{
            puesto_vac:{
                required:"Ingrese la vacante",
                minlength:"Ingrese mínimo 5 caracterers",
                maxlength:"Ingrese máximo 20 caracterers",
                letras:"Ingrese solo letras."
            },
            salario_vac:{
                required:"Ingrese el salario propuesto.",
                minlength:"Ingrese mínimo 3 caracteres",
                maxlength:"Ingrese máximo 10 caracteres",
                number:"Ingrese solo números"
            },
            horas_vac:{
                required:"Ingrese las horas mensuales.",
                minlength:"Ingrese mínimo 1 caracter",
                maxlength:"Ingrese máximo 3 caracteres"
            },
            experiencia_vac:{
                required:"Ingrese los años de experiencia requerida",
                minlength:"Ingrese mínimo 1 caracteres",
                maxlength:"Ingrese máximo 3 caracteres",

            },
            estudios_vac:{
                required:"Ingrese los estudios requeridos",
                minlength:"Ingrese mínimo 3 caracteres",
                maxlength:"Ingrese máximo 15 caracteres",
                letras:"Ingrese solo letras."

            },
            area_vac:{
                required:"Ingrese el área de la vacante",
                minlength:"Ingrese mínimo 5 caracterers",
                maxlength:"Ingrese máximo 15 caracterers",
                letras:"Ingrese solo letras."

            }

        }
    });

</script>
