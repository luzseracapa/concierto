<h1 class="text-center" >EDITAR USUARIO</h1>
<br>
<div style="padding:15px" class="row">
      <form id="frm_editar_usuario" action="<?php echo site_url('Usuarios/procesarActualizacion'); ?>" method="post">
        <div class="card">
            <div class="card-header">
                Usuario
            </div>
            <div class="card-body">
              <div class="col-md-4">
                  <div class="mb-3">
                      <label for="" class="form-label"><b>Perfil:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <select class="form-select form-select-sm" name="perfil_us" id="perfil_us">
                          <option selected>Seleccione el perfil</option>
                          <option value="Administrador">Administrador</option>
                          <option value="Representante">Representante</option>
                      </select>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                      <div class="mb-3">
                        <label for="" class="form-label"</label>
                        <input type="text" value="<?php echo $usuarioEditar->id_us?>" hidden
                          class="form-control" name="id_us" id="id_us" aria-describedby="helpId" placeholder="">
                      </div>
                  </div>
              </div>
            <!-- inicio de cedula usuario-->
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Cédula:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text"value="<?php echo $usuarioEditar->cedula_us?>"
                        class="form-control" name="cedula_us" id="cedula_us" aria-describedby="helpId" placeholder="Número de cedula">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Telefono:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" value="<?php echo $usuarioEditar->telefono_us?>"
                        class="form-control" name="telefono_us" id="telefono_us" aria-describedby="helpId" placeholder="Número de célular">
                    </div>
                </div>
            </div>
            <!-- fin de cedula usuario -->

            <!-- inicio de nombre usuario -->
            <div class="row">

                <div class="col-md-6">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Nombre:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" value="<?php echo $usuarioEditar->nombre_us?>"
                        class="form-control" name="nombre_us" id="nombre_us" aria-describedby="helpId" placeholder="Ingrese el nombre">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Apellido:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" value="<?php echo $usuarioEditar->apellido_us?>"
                        class="form-control" name="apellido_us" id="apellido_us" aria-describedby="helpId" placeholder="Ingrese el apellido">
                    </div>
                </div>
            </div>
            <!-- fin de nombre usuario -->
            <!-- inicio de fecha de nacimiento y genero -->
            <div class="row">

                <div class="col-md-6">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Fecha de nacimiento:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="date" value="<?php echo $usuarioEditar->fechaNac_us?>"
                        class="form-control" name="fechaNac_us" id="fechaNac_us" aria-describedby="helpId" placeholder="">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="" class="form-label"><b>Género:</b></label>
                        <span class="obligatorio">(Obligatorio)</span>
                        <select class="form-select form-select-sm" name="genero_us" id="genero_us" >
                            <option selected>Seleccione el género</option>
                            <option value="Masculino">Masculino</option>
                            <option value="Femenino">Femenino</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Direccion:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" value="<?php echo $usuarioEditar->direccion_us?>"
                        class="form-control" name="direccion_us" id="direccion_us" aria-describedby="helpId" placeholder="Ingrese su direccion">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Email:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" value="<?php echo $usuarioEditar->email_us?>"
                        class="form-control" name="email_us" id="email_us" aria-describedby="helpId" placeholder="Ingrese su email">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Password:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="password" value="<?php echo $usuarioEditar->password_us?>"
                        class="form-control" name="password_us" id="password_us" aria-describedby="helpId" placeholder="Ingrese su password">
                    </div>
                </div>
            </div>
            <!-- fin de fecha de nacimiento y genero -->
            <br>
            <br>
            <!-- inicio botones -->
            <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" name="button" class="btn btn-primary">Editar</button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/usuarios/index" class="btn btn-danger">Cancelar</a>
                    </div>
            </div>
            <br><br>
            <!-- fin botones -->
            </div>
            <div class="card-footer text-muted">
            </div>
        </div>
    </form>
</div>

<!-- vamos hacer las validaciones -->

<script type="text/javascript">
    $("#frm_editar_usuario").validate({
        rules:{
            cedula_us:{
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            },
            telefono_us:{
              required:true,
              number:true,
              minlength:10,
              maxlength:10
            },
            nombre_us:{
                required:true,
                minlength:3,
                maxlength:100,
                letras:true
            },
            apellido_us:{
                required:true,
                minlength:3,
                maxlength:100,
                letras:true
            },
            fechaNac_us:{
                required:true
            },
            genero_us:{
                required:true

            },
            direccion_us:{
              required:true,
              minlength:3,
              maxlength:100,
                letras:true
            },
            email_us:{
              required:true,
              minlength:3,
              maxlength:100,

            },
            password_us:{
              required:true,
              minlength:8,
              maxlength:50,

            },
            perfil_us:{
              required:true

            },


        },
        messages:{
            cedula_us:{
                required:"La cédula es obligatoria",
                number:"Solo se aceptan números",
                minlength:"La cédula debe tener 10 dígitos",
                maxlength:"La cédula debe tener 10 dígitos"
            },
            telefono_us:{
              required:"El telefono es obligatorio",
              number:"Solo se aceptan números",
              minlength:"La cédula debe tener 10 dígitos",
              maxlength:"La cédula debe tener 10 dígitos"
            },
            nombre_us:{
                required:"El nombre es obligatorio",
                minlength:"El nombre debe tener mínimo 3 caracteres",
                maxlength:"El nombre debe tener máximo 100 caracteres"
            },
            apellido_us:{
                required:"El apellido es obligatorio",
                minlength:"El apellido debe tener mínimo 3 caracteres",
                maxlength:"El apellido debe tener máximo 100 caracteres"
            },
            fechaNac_us:{
                required:"La fecha de nacimiento es obligatoria"
            },
            genero_us:{
                required:"El género es obligatorio"
            },
            direccion_us:{
              required:"La direccion es obligatorio",
              minlength:"El apellido debe tener mínimo 3 caracteres",
              maxlength:"El apellido debe tener máximo 100 caracteres"
            },
            email_us:{
              required:"El email es obligatorio",
              minlength:"El apellido debe tener mínimo 3 caracteres",
              maxlength:"El apellido debe tener máximo 100 caracteres",
              email:"El email debe tener un formato válido"
            },
            password_us:{
              required:"Este campo es obligatorio",
              minlength:"La contraseña debe tener mínimo 8 caracteres",
              maxlength:"La contraseña debe tener maximo 50 caracteres"
            },
            perfil_us:{
              required:"Este campo es obligatorio"

            },
        }

    });
</script>
