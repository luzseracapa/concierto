<br><br>
<div  style="padding-left:20px" class="row">
    <div class="col-md-8">
        <h1>Listado de Usuarios</h1>
    </div>
    <div class="col-md-4">
        <a name="" id="" class="btn btn-primary" href=" <?php echo site_url('usuarios/nuevo'); ?>" role="button"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-plus" viewBox="0 0 16 16">
  <path d="M8.5 6a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V10a.5.5 0 0 0 1 0V8.5H10a.5.5 0 0 0 0-1H8.5V6z"/>
  <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
</svg> Agregar Usuario</a>
    </div>
</div>



<?php if($usuarios): ?>
    <div style="padding:25px" class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">CEDULA</th>
                        <th scope="col">NOMBRE</th>
                        <th>APELLIDO</th>
                        <th>DIRECCION</th>
                        <th>EMAIL</th>
                        <th>PASSWORD</th>
                        <th>TELEFONO</th>
                        <th>FECHA DE NACIMIENTO</th>
                        <th>GENERO</th>
                        <th>PPERFIL</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($usuarios as $filatemporal): ?>
                    <tr class="">
                        <td scope="row"><?php echo $filatemporal->id_us ?></td>
                        <td><?php echo $filatemporal->cedula_us ?></td>
                        <td><?php echo $filatemporal->nombre_us ?></td>
                        <td><?php echo $filatemporal->apellido_us ?></td>
                        <td><?php echo $filatemporal->direccion_us ?></td>
                        <td><?php echo $filatemporal->email_us ?></td>
                        <td><?php echo $filatemporal->password_us?></td>
                        <td><?php echo $filatemporal->telefono_us ?></td>
                        <td><?php echo $filatemporal->fechaNac_us?></td>
                        <td><?php echo $filatemporal->genero_us?></td>
                          <td><?php echo $filatemporal->perfil_us?></td>

                        <!-- inicio acciones -->
                        <td>
                                <a href="<?php echo site_url(); ?>/usuarios/editar/<?php echo $filatemporal->id_us?>" title="Editar Usuario" class="btn btn-warning btn-sm"><i class="bi bi-pen-fill"></i></a>

                                <!-- BOTON ELIMINAR -->
                                <a href="<?php echo site_url();?>/usuarios/eliminar/<?php echo $filatemporal->id_us ?>"

                                onclick="return confirm('¿Está seguro de eliminar el registro?')"


                                title="Eliminar Usuario" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>

                        </td>

                    <?php endforeach;?>
                    </tr>

                </tbody>
            </table>
        </div>

    </div>

<?php else: ?>
    <h1>No hay Entrenadores</h1>


<?php endif; ?>
