

<div style="padding:25px" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Cantante
            </div>
            <div class="card-body">
                <!-- inicio del form -->
                <form id="frm_nuevo_entrenador" action="<?php echo site_url(); ?>/entrenadores/guardar" method="post">

                    <div class="row">
                        <!-- inicio de la cedula -->
                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label">Cédula:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="number" required
                                class="form-control" name="cedula_ent" id="" aria-describedby="helpId" placeholder="Cédula de indentidad">
                            </div>

                        </div>
                        <!-- fin de la cedula -->

                        <div class="col-md-6"></div>
                    </div>

                    <div class="row">
                        <!-- inicio nombre -->
                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label">Nombre:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="text" required
                                class="form-control" name="nombre_ent" id="" aria-describedby="helpId" placeholder="Nombre">
                            </div>

                        </div>
                        <!-- fin nombre -->

                        <!-- inicio apellido -->
                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label">Apellido:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="text" required
                                class="form-control" name="apellido_ent" id="" aria-describedby="helpId" placeholder="Apellido">
                            </div>

                        </div>
                        <!-- fin apellido -->

                        <!-- inicio direccion -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                  <label for="" class="form-label">Generó Musical:</label>
                                  <span class="obligatorio">(Obligatorio)</span>
                                  <input type="text"
                                    class="form-control" name="direccion_ent" id="" aria-describedby="helpId" placeholder="Ingrese genero musical">
                                </div>
                            </div>
                        </div>
                        <!-- fin direccion -->

                        <div class="row">
                            <!-- inicio email -->
                            <div class="col-md-6">
                                <div class="mb-3">
                                  <label for="" class="form-label">Email:</label>
                                  <span class="obligatorio">(Obligatorio)</span>
                                  <input type="email"
                                    class="form-control" name="email_ent" id="" aria-describedby="helpId" placeholder="Correo Electrónico">
                                </div>
                            </div>
                            <!-- fin email -->

                            <div class="col-md-6">
                            <!-- inicio telefono -->
                            <div class="mb-3">
                              <label for="" class="form-label">Teléfono:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="number"
                                class="form-control" name="telefono_ent" id="" aria-describedby="helpId" placeholder="Número de teléfono">
                            </div>
                            <!-- fin telefono -->
                            </div>

                        </div>

                    </div>
                    <!-- inicio botones -->
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" name="button"
                            class="btn btn-primary">
                            Guardar
                            </button>
                            &nbsp;
                            <a href="<?php echo site_url(); ?>/entrenadores/index" class="btn btn-danger">
                            Cancelar
                            </a>
                        </div>
                    </div>
                    <!-- fin botones -->
                </form>
                <!-- fin del form -->


            </div>
            <div class="card-footer text-muted">
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" >
    $("#frm_nuevo_entrenador").validate({
        rules:{
            cedula_ent:{
                required:true,
                minlength:10,
                maxlength:13
            },
            nombre_ent:{
                required:true,
                minlength:3,
                maxlength:100,
                letras:true
            },
            apellido_ent:{
                required:true,
                minlength:3,
                maxlength:100,
                letras:true
            },
            direccion_ent:{
                required:true,
                minlength:3,
                maxlength:100,
                letras:true
            },
            email_ent:{
                required:true,
                email:true,
                minlength:3,
                maxlength:100
            },
            telefono_ent:{
                required:true,
                minlength:10,
                maxlength:13
            }
        },messages:{
            cedula_ent:{
                required:"Ingrese la cédula",
                minlength:"Ingrese mínimo 10 números",
                maxlength:"Ingrese máximo 13 números",
                number:"Ingrese solo números"
            },
            nombre_ent:{
                required:"Ingrese el nombre",
                minlength:"Ingrese mínimo 3 caracteres",
                maxlength:"Ingrese máximo 50 caracteres"
            },
            apellido_ent:{
                required:"Ingrese el apellido",
                minlength:"Ingrese mínimo 3 caracteres",
                maxlength:"Ingrese máximo 50 caracteres"
            },
            direccion_ent:{
                required:"Ingrese la dirección",
                minlength:"Ingrese mínimo 3 caracteres",
                maxlength:"Ingrese máximo 100 caracteres"
            },
            email_ent:{
                required:"Ingrese el correo electrónico",
                email:"Ingrese un correo válido",
                minlength:"Ingrese mínimo 3 caracteres",
                maxlength:"Ingrese máximo 50 caracteres"
            },
            telefono_ent:{
                required:"Ingrese el teléfono",
                letras:"Ingrese solo números",
                minlength:"Ingrese mínimo 10 números",
                maxlength:"Ingrese máximo 13 números",
                number:"Ingrese solo números"

            },
        }
    });

</script>
