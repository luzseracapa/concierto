<div style="padding-left:50px; padding-top: 25px;" class="container">
    <div class="row">
        <div class="col-md-8">
            <h3>LISTADO DE ALUMNOS</h3>
        </div>

        <div class="col-md-4">
            <a style="color: black;" href="<?php echo site_url('alumnos/nuevo') ?>" class="btn btn-info btn-sm"><i class="bi bi-person-plus"></i> REGISTRAR ALUMNO</a>
        </div>
    </div>
</div>

<?php if($alumnos): ?>
    <div style="padding:25px" class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table id="exemple" class="table table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">CEDULA</th>
                            <th scope="col">NOMBRE</th>
                            <th>APELLIDO</th>
                            <th>FECHA NACIMIENTO</th>
                            <th>GENERO</th>
                            <th>CIUDAD ESCUELA</th>
                            <th>ESTADO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($alumnos as $filatemporal): ?>
                        <tr class="">
                            <td scope="row"><?php echo $filatemporal->id_alu ?></td>
                            <td><?php echo $filatemporal->cedula_alu ?></td>
                            <td><?php echo $filatemporal->nombre_alu ?></td>
                            <td><?php echo $filatemporal->apellido_alu ?></td>
                            <td><?php echo $filatemporal->fechaNac_alu ?></td>
                            <td><?php echo $filatemporal->genero_alu ?></td>
                            <td><?php echo $filatemporal->ciudad_esc ?></td>
                            <td><?php echo $filatemporal->estado_alu ?></td>
                        <!-- Inicio botones -->
                            <td>
                                <a href="<?php echo site_url(); ?>/alumnos/editar/<?php echo $filatemporal->id_alu?>" title="Editar Alumno" class="btn btn-warning btn-sm"><i class="bi bi-pen-fill"></i></a>

                                <!-- BOTON ELIMINAR -->
                                <a href="<?php echo site_url();?>/alumnos/eliminar/<?php echo $filatemporal->id_alu ?>"

                                onclick="return confirm('¿Está seguro de eliminar el registro?')"


                                title="Eliminar Alumno" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>

                            </td>
                            <!-- Fin botones -->

                        <?php endforeach; ?>
                        </tr>

                    </tbody>
                </table>
            </div>
            
        </div>
    </div>

<?php else: ?>
<h1>No hay alumnos</h1>

<?php endif; ?>
