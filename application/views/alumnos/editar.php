<h1 class="text-center" >EDITAR ALUMNO</h1>
<br>

<div style="padding:15px" class="row">
    <form id="frm_editar_alumno" action="<?php echo site_url('alumnos/procesarActualizacion'); ?>" method="post">
        <div class="card">
            <div class="card-header">
                Alumno
            </div>
            <div class="card-body">

             <!-- vamos a ponr el id -->
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-3">
                    <label for="" class="form-label"></label>
                    <!-- vamos agregar el value para que jale los datos de id  -->
                    <!-- vamos a ponerle oculto para que no se vea con hidden para que no se muestre-->
                    <input type="hidden" value="<?php echo $escuelaEditar->id_alu?>"
                        class="form-control" name="id_alu" id="id_alu" aria-describedby="helpId" placeholder="">
                    </div>
                </div>
            </div>
            <!-- fi del id -->

            <!-- inicio de cedula alumno -->
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Cédula:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" value="<?php echo $escuelaEditar->cedula_alu?>"
                        class="form-control" name="cedula_alu" id="cedula_alu" aria-describedby="helpId" placeholder="Número de cédula">
                    </div>
                </div>
            </div>
            <!-- fin de cedula alumno -->


            <!-- inicio de nombre alumno -->
            <div class="row">

                <div class="col-md-6">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Nombre:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" value="<?php echo $escuelaEditar->nombre_alu?>"
                        class="form-control" name="nombre_alu" id="nombre_alu" aria-describedby="helpId" placeholder="Ingrese el nombre">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Apellido:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" value="<?php echo $escuelaEditar->apellido_alu?>"
                        class="form-control" name="apellido_alu" id="apellido_alu" aria-describedby="helpId" placeholder="">
                    </div>
                </div>
            </div>
            <!-- fin de nombre alumno -->

            <!-- inicio de fecha de nacimiento y genero -->
            <div class="row">

                <div class="col-md-6">
                    <div class="mb-3">
                      <label for="" class="form-label"><b>Fecha de nacimiento:</b></label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="date" value="<?php echo $escuelaEditar->fechaNac_alu?>"
                        class="form-control" name="fechaNac_alu" id="fechaNac_alu" aria-describedby="helpId" placeholder="">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="genero_alu" class="form-label"><b>Género:</b></label>
                        <span class="obligatorio">(Obligatorio)</span>
                        <select class="form-select form-select" name="genero_alu" id="genero_alu">
                            <option value="">Seleccione el género</option>
                            <option value="masculino" <?php if ($genero_alu == 'masculino') echo "selected"; ?>>Masculino</option>
                            <option value="femenino" <?php if ($genero_alu == 'femenino') echo "selected"; ?>>Femenino</option>
                        </select>
                    </div>
                </div>

            </div>
            <!-- fin de fecha de nacimiento y genero -->

            <div class="row"> <!-- inicio row -->

            <!-- inicio id escuela -->
            <div class="col-md-6">
                <div class="mb-3">
                    <label for="id_escuela" class="form-label"><b>Ciudad de la Escuela:</b></label>
                    <span class="obligatorio">(Obligatorio)</span>
                    <select class="form-control" name="id_escuela" id="id_escuela" aria-describedby="helpId">
                        <option value="">Seleccione la ciudad</option>
                        <?php foreach ($escuelas as $escuela) { ?>
                            <option value="<?php echo $escuela->id_esc; ?>" <?php if ($escuela->id_esc == $valorSeleccionado) echo 'selected'; ?>><?php echo $escuela->ciudad_esc; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <!-- fin id escuela -->

            <!-- inicio estado alumno -->
            <div class="col-md-6">
                <div class="mb-3">
                    <label for="estado_alu" class="form-label"><b>Estado:</b></label>
                    <span class="obligatorio">(Obligatorio)</span>
                    <select class="form-select form-select" name="estado_alu" id="estado_alu">
                        <option value="">Seleccione el estado</option>
                        <option value="activo" <?php if ($estado_alu == 'activo') echo 'selected'; ?>>Activo</option>
                        <option value="inactivo" <?php if ($estado_alu == 'inactivo') echo 'selected'; ?>>Inactivo</option>
                    </select>
                </div>
            </div>

            <!-- fin estado alumno -->


            </div> <!-- fin row -->

            <!-- inicio botones -->
            <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" name="button" class="btn btn-primary">Editar</button>
                        &nbsp;
                        <a href="<?php echo site_url(); ?>/alumnos/index" class="btn btn-danger">Cancelar</a>
                    </div>
            </div>
            <br><br>
            <!-- fin botones -->



            </div>
            <div class="card-footer text-muted">
            </div>
        </div>
    </form>
</div>

<!-- vamos hacer las validaciones -->

<script type="text/javascript">
    $("#frm_editar_alumno").validate({
        rules:{
            cedula_alu:{
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            },
            nombre_alu:{
                required:true,
                minlength:3,
                maxlength:100,
                letras:true
            },
            apellido_alu:{
                required:true,
                minlength:3,
                maxlength:100,
                letras:true
            },
            fechaNac_alu:{
                required:true
            },
            genero_alu:{
                required:true
            },
            id_escuela:{
                required:true
            },
            estado_alu:{
                required:true
            }
        },
        messages:{
            cedula_alu:{
                required:"La cédula es obligatoria",
                number:"Solo se aceptan números",
                minlength:"La cédula debe tener 10 dígitos",
                maxlength:"La cédula debe tener 10 dígitos"
            },
            nombre_alu:{
                required:"El nombre es obligatorio",
                minlength:"El nombre debe tener mínimo 3 caracteres",
                maxlength:"El nombre debe tener máximo 100 caracteres"
            },
            apellido_alu:{
                required:"El apellido es obligatorio",
                minlength:"El apellido debe tener mínimo 3 caracteres",
                maxlength:"El apellido debe tener máximo 100 caracteres"
            },
            fechaNac_alu:{
                required:"La fecha de nacimiento es obligatoria"
            },
            genero_alu:{
                required:"El género es obligatorio"
            },
            id_escuela:{
                required:"La escuela es obligatoria"
            },
            estado_alu:{
                required:"El estado es obligatorio"
            }
        }

    });
</script>