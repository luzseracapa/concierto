

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h3>LISTADO DE CANCHAS</h3>
        </div>

        <div class="col-md-4">
            <a style="color: black;" href="<?php echo site_url('canchas/nuevaCancha') ?>" class="btn btn-info btn-sm"><i class="bi bi-person-plus"></i> REGISTRAR CANCHA</a>
        </div>
    </div>
</div>


<?php if($canchas): ?>

<div class="container">
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">NOMBRE</th>
                    <th scope="col">PAIS</th>
                    <th>TELEFONO</th>
                    <th>SUPERFICIE</th>
                    <th>AFORO/MAX</th>
                    <th>LATITUD</th>
                    <th>LONGITUD</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach($canchas as $filatemporal): ?>
                <tr class="">
                    <td scope="row"><?php echo $filatemporal->id_can ?></td>
                    <td><?php echo $filatemporal->nombre_can?></td>
                    <td><?php echo $filatemporal->pais_can?></td>
                    <td><?php echo $filatemporal->telefono_can?></td>
                    <td><?php echo $filatemporal->superficie_can?></td>
                    <td><?php echo $filatemporal->aforo_can?></td>
                    <td><?php echo $filatemporal->latitud_can?></td>
                    <td><?php echo $filatemporal->longitud_can?></td>
                    <td>
                              <!-- BOTON GUARDAR -->
                              <a href="<?php echo site_url(); ?>/canchas/editarCancha/<?php echo $filatemporal->id_can?>" title="Editar Cancha" class="btn btn-warning btn-sm"><i class="bi bi-pen-fill"></i></a>

                                <!-- BOTON ELIMINAR -->
                                <a href="<?php echo site_url();?>/canchas/eliminar/<?php echo $filatemporal->id_can ?>"

                                onclick="return confirm('¿Está seguro de eliminar el registro?')"


                                title="Eliminar Cliente" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>

                            </td>
                <?php endforeach; ?>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<?php else: ?>
    <h1>No hay sucursales</h1>

<?php endif; ?>
