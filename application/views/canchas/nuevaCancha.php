
<div class="container">
  <div style="padding:25px" class="row">
      <div class="col-md-12">
        <h1>REGISTRO DE CANCHAS</h1>

          <div class="card">
              <div class="card-header">
                  Formulario registro de canchas.
              </div>
              <div class="card-body">
                  <!-- inicio del form -->
                  <form id="frm_nuevo_cancha"
                  action="<?php echo site_url(); ?>/canchas/guardar"
                  method="post">
                        <div class="row">
                              <div class="col-md-6">
                                  <label for="">SUPERFICIE DEL CAPO:</label>
                                  <span class="obligatorio">(Obligatorio)</span>
                                  <br>
                                  <select type="text" id="superficie_can" name="superficie_can" value"" >
                                  <option value "">Selecione una opción</option> slot
                                  <option value="Tierra">Tierra</option> slot
                                  <option value="Cemento ">Cemento</option> slot
                                  <option value="Cesped_natural">Cesped natural</option> slot
                                  <option value="Cesped_sintetico">Cesped sintetico</option> slot
                                  </select>
                              </div>
                        </div>
                        <br>
                      <div class="row">
                        <div class="col-md-6">
                          <label for="" class="form-label">NOMBRE:</label>
                            <span class="obligatorio">(Obligatorio)</span>
                            <br>
                            <input type="text"
                            placeholder="Ingrese nombre de la cancha"
                            class="form-control"

                            name="nombre_can" value=""
                            id="nombre_can">
                        </div>
                        <div class="col-md-6">
                            <label for="">PAIS:
                            </label>
                            <span class="obligatorio">(Obligatorio)</span>
                            <br>
                            <input type="text"
                            placeholder="Ingrese país de ubicación de cancha."

                            class="form-control"
                            name="pais_can" value=""
                            id="pais_can">
                        </div>

                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-6">
                          <label for="">TELÉFONO:</label>
                          <span class="obligatorio">(Obligatorio)</span>
                          <br>
                          <input type="number"
                          placeholder="Ingrese teléfono de contacto."
                          class="form-control"
                          name="telefono_can" value=""
                          id="telefono_can">
                        </div>
                        <div class="col-md-6">
                            <label for="">AFORO:
                            </label>
                            <span class="obligatorio">(Obligatorio)</span>
                            <br>
                            <input type="number"
                            placeholder="Ingrese el aforo de la cancha."
                            class="form-control"
                            required
                            name="aforo_can" value=""
                            id="aforo_can">
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label"><b>LATITUD:</b></label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="float" min="-0.93859" max="90" step="0.0001"  readonly
                                class="form-control" name="latitud_can" id="latitud" aria-describedby="helpId" placeholder="Selecciona la ubicación en el mapa">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label"><b>LONGITUD:</b></label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="float" min="-180.0000" max="180" step="0.0001"  readonly
                                class="form-control" name="longitud_can" id="longitud" aria-describedby="helpId" placeholder="Selecciona la ubicación en el mapa" >
                            </div>
                        </div>

                    </div>


                      <br>
                      <!-- estamos colocando el espacio para poner el mapa -->
                      <div style="padding: 22px;" class="row">
                      <div class="col-md-12">
                          <div id="mapaubicaciónCancha" style="width: 100%; height: 500px; border:2px solid black;"></div>
                      </div>
                      </div>
                      <!-- fin de colocando espacio para poner el mapa -->
                      <!-- fin row para laitus y longitud -->

                      <br>
                      <div class="row">
                          <div class="col-md-12 text-center">
                              <button type="submit" name="button"
                              class="btn btn-primary">
                              guardar
                              </button>
                              &nbsp;
                              <a href="<?php echo site_url(); ?>/canchas/listarCancha"
                                class="btn btn-danger">
                                Cancelar
                              </a>
                          </div>
                      </div>
                  </form>


                  <!-- fin del form -->
              </div>
              <div class="card-footer text-muted">
              </div>
          </div>
      </div>
  </div>


            <!-- instancia del mapa -->
            <!-- estamos instanciando el mapa de ubicación para que se registre solo tocando la ubicación -->
            <script type="text/javascript">
                function initMap(){
                  var centro = new google.maps.LatLng(-1.6364025532680684, -78.65209578103213);

                  var mapa= new google.maps.Map(
                    document.getElementById("mapaubicaciónCancha"),
                    {
                      center: centro,
                      zoom: 7,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                    }

                  );

                  var marcador = new google.maps.Marker({
                      position: centro,
                      title: "Centro de la ciudad",
                      map: mapa,
                      draggable: true,

                      icon: "<?php echo base_url(); ?>assets/images/icon4.png"
                    });

                    google.maps.event.addListener(marcador, 'dragend', function(){ //cuando termine la arrastrada del marcador, se ejecuta la funcion

                      document.getElementById("latitud").value=this.getPosition().lat();
                      document.getElementById("longitud").value=this.getPosition().lng();
                    });
                } //cierre de la funcion
            </script>
            <!-- fin de la funcion -->

            <!-- validaciones -->
            <script type="text/javascript" >
            $("#frm_nuevo_cancha").validate({
                rules:{
                        nombre_can:{
                        required:true,
                        minlength:5,
                        maxlength:250,
                        letras:true
                      },
                      pais_can:{
                          required:true,
                          minlength:5,
                          maxlength:250,
                          letras:true
                      },
                      telefono_can:{
                          required:true,
                          minlength:10,
                          maxlength:10,
                          digits:true
                      },
                      superficie_can:{
                          required:true
                      },
                      aforo_can:{
                      required:true,
                      minlength:1,
                      maxlength:10,
                      digits:true
                    },
                    latitud_can:{
                        required:true,
                        minlength:10,
                        maxlength:100
                    },
                    longitud_can:{
                        required:true,
                        minlength:10,
                        maxlength:100
                    }
                },
                messages:{
                    nombre_can:{
                      required:"Ingrese el nombre de la cancha.",
                        minlength:"Ingrese al menos 5 caracteres",
                        maxlength:"caracteres maximos 250",
                          letras:"Solo se aceptan letras"
                        },
                        pais_can:{
                            required:"Por favor ingrese el pais de la cancha.",
                            minlength:"Ingrese al menos 5 caracteres",
                            maxlength:"caracteres maximos 250",
                            letras:"Solo se aceptan letras"
                        },
                        telefono_can:{
                            required:"Por favor ingrese contacto de la cancha.",
                            minlength:"Ingrese al menos 10 caracteres",
                            maxlength:"Caracteres maximos 10",
                            number:"Solo se aceptan números."
                        },
                        superficie_can:{
                            required:"<-- Por favor selecione una opción.",
                        },
                        aforo_can:{
                            required:"Por favor ingrese aforo de la cancha.",
                            minlength:"Ingrese al menos 1 caracter",
                            maxlength:"caracteres maximos 10"
                        },

                        latitud_can:{
                            required:"Por favor Selecione la latitud de la cancha en el mapa.",

                        },
                        longitud_can:{
                            required:"Por favor seleccione la longitud de la cancha en el mapa.",

                        },
                    }
            });
            </script>
