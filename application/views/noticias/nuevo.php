
<div style="padding:25px" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Noticias
            </div>

            <div class="card-body">
                <!-- inicio del post -->
                <form id="frm_nueva_noticia"  action="<?php echo site_url(); ?>/Noticias/guardar" method="post">

                <div class="col-md-6">
                <div class="mb-4">
                    <label for="" class="form-label"><b>Estado:</b></label>
                    <span class="obligatorio">(Obligatorio)</span>
                    <select class="form-select form-select-lg" name="estado" id="estado">
                        <option selected>Seleccione el estado</option>
                        <option value="activa">activa</option>
                        <option value="inactiva">inactiva </option>
                          <option value="archivada">archivada</option>
                    </select>
                    </div>
                  </div>
                    <!-- inicio barrio -->
                    <div class="mb-3">
                      <label for="" class="form-label">Titulo:</label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text"
                        class="form-control" name="titulo" id="titulo" aria-describedby="helpId" placeholder="">
                    </div>
                    <!-- fin barrio -->

                    <!-- inicio ciudad -->
                    <div class="mb-3">
                      <label for="" class="form-label">Contenido:</label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" style="text-align:center"
                        class="form-control" name="contenido" id="contenido" aria-describedby="helpId" placeholder="">
                    </div>
                    <!-- fin ciudad -->

                    <div class="row">
                        <!-- inicio telefono -->
                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label"><b>Fecha de publicacion:</b></label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="date"
                                class="form-control" name="fecha_publicacion" id="fecha_publicacion" aria-describedby="helpId" placeholder="">
                            </div>
                        </div>
                        <!-- fin telefono -->

                        <div class="col-md-6">
                        <div class="mb-3">
                            <label for="" class="form-label"><b>Categoria:</b></label>
                            <span class="obligatorio">(Obligatorio)</span>
                            <select class="form-select form-select-lg" name="categoria" id="categoria">
                                <option selected>Seleccione la categoria</option>
                                <option value="Entrenamientos">Entrenamientos</option>
                                <option value="Partidos">Partidos</option>
                                <option value="Eventos">Eventos</option>
                            </select>
                        </div>
                    </div>
                  </div>

                    <!-- inicio usuario -->
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mb-3">
                              <label for="id_us" class="form-label">ID Usuario:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <select class="form-control" name="id_us" id="id_us" required>
                                <option value="">Seleccione el Autor</option>
                                <?php foreach ($usuarios as $usuario){ ?>
                                    <option value="<?php echo $usuario->id_us; ?>"><?php echo $usuario->nombre_us . ' ' . $usuario->apellido_us; ?></option>

                                <?php } ?>

                              </select>
                            </div>
                        </div>
                    </div>
                    <!-- fin usuario -->



                        <!-- inicio botones -->
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                                &nbsp;
                                <a href="<?php echo site_url(); ?>/noticias/index" class="btn btn-danger">Cancelar</a>
                            </div>
                        </div>
                        <br><br>
                        <!-- fin botones -->



                </form>
                <!-- fin del post -->
            </div>
            <div class="card-footer text-muted">
            </div>

        </div>
    </div>
</div>

<script type="text/javascript" >
  $("#frm_nueva_noticia").validate({
    rules:{
      titulo:{
        required: true,
        minlength: 3,
        maxlength: 150,
        letras: true
      },
      contenido:{
        required: true,
        minlength: 3,
        maxlength: 150,
        letras: true
      },
      fecha_publicacion:{
        required: true,

      },
      id_us:{
        required: true
      }
    },
    messages:{
      titulo:{
        required: "El campo es obligatorio",
        minlength: "El campo debe tener minimo 3 caracteres",
        maxlength: "El campo debe tener maximo 150 caracteres",
        letras: "El campo solo debe tener letras"
      },
      contenido:{
        required: "El campo es obligatorio",
        minlength: "El campo debe tener minimo 3 caracteres",
        maxlength: "El campo debe tener maximo 150 caracteres",
        letras: "El campo solo debe tener letras"
      },
      fecha_publicacion:{
        required: "El campo es obligatorio",
      },
      id_us:{
        required: "El campo es obligatorio"
      }

    }

  });

</script>
