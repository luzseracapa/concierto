<div style="padding-left:50px; padding-top: 25px;" class="container">
    <div class="row">
        <div class="col-md-8">
            <h3>LISTADO DE NOTICIAS</h3>
        </div>

        <div class="col-md-4">
            <a style="color: black;" href="<?php echo site_url('noticias/nuevo') ?>" class="btn btn-info btn-sm"><i class="bi bi-person-plus"></i> INGRESAR NOTICIA</a>
        </div>
    </div>
</div>

<?php if($noticias): ?>
    <div style="padding:25px" class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">TITULO</th>
                            <th scope="col">CONTENIDO</th>
                            <th>FECHA DE PUBLICACION</th>
                            <th>CATEGORIA</th>
                            <th>ESTADO</th>
                            <th>AUTOR</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($noticias as $filatemporal): ?>
                        <tr class="">
                            <td scope="row"><?php echo $filatemporal->id_noticia ?></td>
                            <td><?php echo $filatemporal->titulo ?></td>
                            <td><?php echo $filatemporal->contenido ?></td>
                            <td><?php echo $filatemporal->fecha_publicacion ?></td>
                            <td><?php echo $filatemporal->categoria ?></td>
                            <td><?php echo $filatemporal->estado ?></td>
                            <td><?php echo $filatemporal->nombre_us .' '. $filatemporal->apellido_us ?></td>

                            <!-- Inicio botones -->
                            <td>
                                <a href="<?php echo site_url(); ?>/noticias/editar/<?php echo $filatemporal->id_noticia ?>" title="Editar Noticias" class="btn btn-warning btn-sm"><i class="bi bi-pen-fill"></i></a>
                                <!-- BOTON ELIMINAR -->
                                <a href="<?php echo site_url();?>/noticias/eliminar/<?php echo $filatemporal->id_noticia ?>"

                                onclick="return confirm('¿Está seguro de eliminar el registro?')"


                                title="Eliminar Noticia" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>

                           </td>
                            <!-- Fin botones -->
                        <?php endforeach; ?>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>

<?php else: ?>
    <h1>No hay ningun registro de Noticias!</h1>

<?php endif; ?>
