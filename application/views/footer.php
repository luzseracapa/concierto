 <!-- validamos si exisite un flash data que se llame confirmacion solo si existe va salir hola mundo -->

 <!-- Bienvenida -->
 <?php if ($this->session->flashdata("bienvenida")): ?>
   <script type="text/javascript">
 	toastr.info("<?php echo
 	$this->session->flashdata("bienvenida"); ?>");
   </script>
   <?php $this->session
      	->set_flashdata("bienvenida","")  ?>
 <?php endif; ?>

<?php if ($this->session->flashdata("confirmacion")): ?>
  <script type="text/javascript">
	toastr.success("<?php echo $this->session->flashdata('confirmacion'); ?>");
  </script>

<?php $this->session->set_flashdata("confirmacion","") ?>
<?php endif; ?>

<!-- invocamos la funcion para mensajes de error con toastr -->
<?php if ($this->session->flashdata("error")): ?>
  <script type="text/javascript">
	toastr.error("<?php echo $this->session->flashdata('error'); ?>");
  </script>

<?php $this->session->set_flashdata("error","") ?>
<?php endif; ?>

<!-- mensaje de editar instructores -->
<?php if ($this->session->flashdata("editar")): ?>
  <script type="text/javascript">
	toastr.info("<?php echo $this->session->flashdata('editar'); ?>");
  </script>

<?php $this->session->set_flashdata("editar","") ?>
<?php endif; ?>





<style media="screen">
  .obligatorio{
    color:red;
    background-color:white;
    border-radius:20px;
    font-size: 12px;
    padding-left: 5px;
    padding-right: 5px;
  }
  .error{
    color:red;
    font-weight: bold;
  }
  input.error{
    border: 2px solid red;
  }
</style>
<!-- Footer Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary rounded-top p-4">
            <div class="row">
                <div class="col-12 col-sm-6 text-center text-sm-start">
                    &copy; <a href="#">Your Site Name</a>, All Right Reserved.
                </div>
                <div class="col-12 col-sm-6 text-center text-sm-end">
                    <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                    Designed By <a href="https://htmlcodex.com">HTML Codex</a>
                    <br>Distributed By: <a href="https://themewagon.com" target="_blank">ThemeWagon</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->
        </div>
        <!-- Content End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url('plantilla/lib/chart/chart.min.js'); ?>"></script>
    <script src="<?php echo base_url('plantilla/lib/easing/easing.min.js'); ?>"></script>
    <script src="<?php echo base_url('plantilla/lib/waypoints/waypoints.min.js'); ?>"></script>
    <script src="<?php echo base_url('plantilla/lib/owlcarousel/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo base_url('plantilla/lib/tempusdominus/js/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('plantillalib/tempusdominus/js/moment-timezone.min.js'); ?>"></script>
    <script src="<?php echo base_url('plantilla/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js'); ?>"></script>

    <!-- Template Javascript -->
    <script src="<?php echo base_url('plantilla/js/main.js'); ?>"></script>

    <!-- configuracion video pausar -->
    <script>
        var videoPlayer = document.getElementById('video-player');

        function toggleVideo() {
            if (videoPlayer.paused) {
            videoPlayer.play();
            } else {
            videoPlayer.pause();
            }
        }
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('table').DataTable({
        "pageLength": 5,
        "lengthMenu": [5, 10, 25, 50],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se encontraron resultados en su búsqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "buttons": [
            'copy', 'excel', 'pdf', 'print'
        ]
    });
});

</script>




</body>

</html>
