

<div style="padding:25px" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                NUEVA SOLICITUD
            </div>

            <div class="card-body">
                <!-- inicio del post -->
                <form id="frm_nuevo_solicitud" action="<?php echo site_url(); ?>/solicitudes/procesarActualizacion" method="post">
                  <!-- inicio de id -->
                  <div class="row">
                      <div class="col-md-4">
                          <div class="mb-3">
                            <label for="" class="form-label">ID:</label>
                            <input type="text" value="<?php echo $solicitudEditar->id_sol?>" readonly
                              class="form-control" name="id_sol" id="id_sol" aria-describedby="helpId" placeholder="">
                            <small id="helpId" class="form-text text-muted">Help text</small>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                    <!-- inicio Nombre -->
                    <div class="col-md-6">
                      <label for="" class="form-label">Nombre:</label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" value="<?php echo $solicitudEditar->nombre_sol?>"
                        class="form-control" name="nombre_sol" id="nombre_sol" aria-describedby="helpId" placeholder="Nombre de postulante.">
                    </div>
                    <!-- fin Nombre -->

                    <!-- inicio Apellido -->
                    <div class="col-md-6">
                      <label for="" class="form-label">Apellido:</label>
                      <span class="obligatorio">(Obligatorio)</span>
                      <input type="text" value="<?php echo $solicitudEditar->apellido_sol?>"
                        class="form-control" name="apellido_sol" id="apellido_sol" aria-describedby="helpId" placeholder="Apellido de postulante">
                    </div>
                    <!-- fin Apellido -->
                  </div>


                    <div class="row">
                        <!-- inicio telefono -->
                        <div class="col-md-6">
                            <div class="mb-3">
                              <label for="" class="form-label">Teléfono:</label>
                              <span class="obligatorio">(Obligatorio)</span>
                              <input type="number" value="<?php echo $solicitudEditar->telefono_sol?>"
                                class="form-control" name="telefono_sol" id="telefono_sol" aria-describedby="helpId" placeholder="Telefono de postulante.">
                            </div>
                        </div>
                        <!-- fin telefono -->

                        <div class="col-md-6">
                          <label for="" class="form-label">Estudios realizados:</label>
                          <span class="obligatorio">(Obligatorio)</span>
                          <input type="text" value="<?php echo $solicitudEditar->estudios_sol?>"
                            class="form-control" name="estudios_sol" id="estudios_sol" aria-describedby="helpId" placeholder="Estudios del postulante.">
                        </div>
                    </div>

                    <!-- inicio entrenador -->
                    <div class="row">
                      <div class="col-md-6">
                        <label for="" class="form-label">Salario Esperado:</label>
                        <span class="obligatorio">(Obligatorio)</span>
                        <input type="number" value="<?php echo $solicitudEditar->salario_sol?>"
                          class="form-control" name="salario_sol" id="salario_sol" aria-describedby="helpId" placeholder="Salario aspirado.">
                      </div>


                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="vacante_id" class="form-label">ID Solicitud:</label>
                                <span class="obligatorio">(Obligatorio)</span>
                                <select class="form-control" name="vacante_id" id="vacante_id" required>
                                    <option value="">Seleccione el entrenador</option>
                                    <?php foreach ($vacantes as $vacante){ ?>
                                        <option value="<?php echo $vacante->id_vac; ?>" <?php if ($vacante->id_vac == $valorSeleccionado) echo "selected"; ?>><?php echo $vacante->puesto_vac ; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>


                    </div>
                    <!-- fin solicitud -->
                    <!-- inicio botones -->
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" name="button" class="btn btn-primary">Editar</button>
                            &nbsp;
                            <a href="<?php echo site_url(); ?>/solicitudes/listarSolicitud" class="btn btn-danger">Cancelar</a>
                        </div>
                    </div>
                    <br><br>
                    <!-- fin botones -->



                </form>
                <!-- fin del post -->
            </div>
            <div class="card-footer text-muted">
            </div>

        </div>
    </div>
</div>


<script type="text/javascript" >
  $("#frm_nuevo_solicitud").validate({
    rules:{
      nombre_sol:{
        required: true,
        minlength: 3,
        maxlength: 150,
        letras: true
      },
      apellido_sol:{
        required: true,
        minlength: 3,
        maxlength: 150,
        letras: true
      },
      telefono_sol:{
        required: true,
        minlength: 10,
        maxlength: 13
      },
      estudios_sol:{
        required: true,
        minlength: 5,
        maxlength: 150,
        letras: true
      },
      salario_sol:{
        required: true,
        minlength: 1,
        maxlength: 6
      },
      vacante_id:{
        required: true
      },

    },
    messages:{
      nombre_sol:{
        required: "El campo es obligatorio",
        minlength: "El campo debe tener minimo 3 caracteres",
        maxlength: "El campo debe tener maximo 150 caracteres",
        letras: "El campo solo debe tener letras"
      },
      ciudad_esc:{
        required: "El campo es obligatorio",
        minlength: "El campo debe tener minimo 3 caracteres",
        maxlength: "El campo debe tener maximo 150 caracteres",
        letras: "El campo solo debe tener letras"
      },
      telefono_sol:{
        required: "El campo es obligatorio",
        minlength: "El campo debe tener minimo 10 caracteres",
        maxlength: "El campo debe tener maximo 13 caracteres",
        number: "El campo solo debe tener numeros"
      },
      estudios_sol:{
        required: "El campo es obligatorio",
        minlength: "El campo debe tener minimo 3 caracteres",
        maxlength: "El campo debe tener maximo 150 caracteres",
        letras: "El campo solo debe tener letras"
      },
      salario_sol:{
        required: "El campo es obligatorio",
        minlength: "El campo debe tener minimo 1 caracteres",
        maxlength: "El campo debe tener maximo 6 caracteres",
        number: "El campo solo debe tener numeros"
      },
      vacante_id:{
        required: "El campo es obligatorio"
      },


    }

  });

</script>
