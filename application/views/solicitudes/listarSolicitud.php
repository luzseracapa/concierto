<div style="padding-left:50px; padding-top: 25px;" class="container">
    <div class="row">
        <div class="col-md-8">
            <h3>LISTADO DE SOLICITUDES</h3>
        </div>

        <div class="col-md-4">
            <a style="color: black;" href="<?php echo site_url('solicitudes/nuevaSolicitud') ?>" class="btn btn-info btn-sm"><i class="bi bi-person-plus"></i> REGISTRAR SOLICITUD</a>
        </div>
    </div>
</div>

<?php if($solicitudes): ?>
    <div style="padding:25px" class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">NOMBRE</th>
                            <th scope="col">APELLIDO</th>
                            <th>TELEFONO</th>
                            <th>ESTUDIOS</th>
                            <th>SALARIO</th>
                            <th>VACANTE</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($solicitudes as $filatemporal): ?>
                        <tr class="">
                            <td scope="row"><?php echo $filatemporal->id_sol ?></td>
                            <td><?php echo $filatemporal->nombre_sol ?></td>
                            <td><?php echo $filatemporal->apellido_sol ?></td>
                            <td><?php echo $filatemporal->telefono_sol ?></td>
                            <td><?php echo $filatemporal->estudios_sol ?></td>
                            <td><?php echo $filatemporal->salario_sol ?></td>
                            <td><?php echo $filatemporal->puesto_vac ?></td>

                            <!-- Inicio botones -->
                            <td>
                                <a href="<?php echo site_url(); ?>/solicitudes/editarSolicitud/<?php echo $filatemporal->id_sol ?>" title="Editar Solicitud" class="btn btn-warning btn-sm"><i class="bi bi-pen-fill"></i></a>

                                <!-- BOTON ELIMINAR -->
                                <a href="<?php echo site_url();?>/solicitudes/eliminar/<?php echo $filatemporal->id_sol ?>"

                                onclick="return confirm('¿Está seguro de eliminar el registro?')"


                                title="Eliminar Solicitud" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></a>

                            </td>
                            <!-- Fin botones -->
                        <?php endforeach; ?>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>

<?php else: ?>
    <h1>No hay pedidos</h1>

<?php endif; ?>
