<style>
    .card-text {
        text-align: center;
    }
</style>

<!-- video con carrausel -->
  <!-- Inner -->
	<div class="carousel-inner">
	<!-- Single item -->
	<div class="">
		<video id="video-player" class="img-fluid" autoplay loop muted controls>
		<source src="<?php echo base_url(); ?>/assets/images/bocha-video.mp4" type="video/mp4" />
		</video>
		<div class="carousel-caption d-none d-md-block">
		<h5 style="font-size:50px; text-shadow: 0 0 5px white;">Sacamos tu mejor potencial</h5>
		<p>
			El esfuerzo de hoy es el éxito de mañana.
		</p>
		</div>
	</div>
	</div>


	<hr>
<!-- Carousel wrapper -->
<div class="container" id="miCard">
	<div class="row">
		<div class="col-md-6">
			<img style="border-radius: 20px;" src="https://itsf.edu.ec/wp-content/uploads/2020/12/MISION-768x576-1.jpg" alt="" width="90%" height="400px">
		</div>
		<div class="col-md-6">
			<img style="border-radius: 20px;"src="https://itsf.edu.ec/wp-content/uploads/2020/12/VISION1-768x576-1.jpg" alt="" width="90%" height="400px">
		</div>
	</div>
</div>
<br>

<hr>
<div class="container" >
	<div class="row">
		<div class="col-md-4">
			<div class="card" style="width: 18rem;">
		  <img src="https://soccerinteraction.academy/sites/default/files/styles/img_bloque_media/public/2021-05/im-british.jpg?itok=D9fN6hLj" class="card-img-top" alt="...">
		  <div class="card-body">
				<h3 class="text-center">NUESTRAS INSTALACIONES</h3>
		    <p class="card-text">"Nuestra escuela de fútbol cuenta con instalaciones modernas y diseñadas específicamente para brindar a
					nuestros jugadores el mejor entorno para su desarrollo futbolístico."</p>
		  </div>
		</div>
		</div>

		<div class="col-md-4">
			<div class="card" style="width: 18rem;">
		  <img src="https://grb.edu.ec/wp-content/uploads/2021/04/campus1-1-scaled.jpg" class="card-img-top" alt="...">
		  <div class="card-body">
				<h3 class="text-center">NUESTRAS CANCHAS</h3>
		    <p class="card-text">"En nuestra escuela de fútbol, contamos con modernas instalaciones deportivas que incluyen varias
					canchas diseñadas para el entrenamiento y desarrollo de nuestros talentosos jugadores."</p>
		  </div>
		</div>
		</div>
		<div class="col-md-4">
			<div class="card" style="width: 18rem;">
			<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHqidObMn63FrSxFIQlJP996byT0bhAQs4v9mu1p9sWeFdZCkfU_8wCZrCvTKkpyMXutg&usqp=CAU" class="card-img-top" alt="...">
			<div class="card-body">
				<h3 class="text-center">NUESTRO PERSONAL</h3>
				<p class="card-text">"Nuestra escuela de fútbol se enorgullece de contar con un equipo de profesionales altamente capacitados
					y apasionados por el fútbol, está compuesto por profesionales que poseen una profunda comprensión del juego."</p>
			</div>
		</div>
		</div>
    
			<!-- cierre de row -->
		</div>
    <br>
    <br>
    <div class="row">
  		<div class="col-md-4">
  			<div class="card" style="width: 18rem;">
  		  <img src="https://scontent.fatf4-1.fna.fbcdn.net/v/t39.30808-6/302611906_750392496137822_7230381170920327818_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=e3f864&_nc_eui2=AeHSdsWvYxcRA-vFUlYO6QvbYQJoJzhsTKdhAmgnOGxMp0AD5uZnEdFPjFmBfAq-fqA9RzAwHBgDCXVLxSTZ0Gr1&_nc_ohc=zmGsaRTuTZMAX8cL8lz&_nc_ht=scontent.fatf4-1.fna&oh=00_AfCqERGQoqdRtO9kn5jzgF_vfGJPw-vFiZ6n6LkilsAdQQ&oe=64A13CE3" class="card-img-top" alt="...">
  		  <div class="card-body">
  				<h3 class="text-center">¡TODAS LAS PROPUESTAS DE ESTE AÑO!</h3>
      		    <p class="card-text">"
            ⚽️ Fútbol masculino y femenino
			Todas las categorias
			Con los mejores entrenadores

            Inscripciones en la Secretaría del Club de lunes a viernes de 9h00 a 17h00."
          </p>
  		  </div>
  		</div>
  		</div>

  		<div class="col-md-4">
  			<div class="card" style="width: 18rem;">
  		  <img src="https://scontent.fatf4-1.fna.fbcdn.net/v/t1.6435-9/126990250_3441744002569369_2020560515244536675_n.png?stp=dst-png_s600x600&_nc_cat=100&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeFU83qjT5p6i_rc3Ch0ylMcF-nl0GxSEi0X6eXQbFISLbjK4Z5k7eD9KFpVkbTg4g5-x_lVRIRlJaxfSCghpuND&_nc_ohc=mnNs-d0VUC8AX-1JmVi&_nc_oc=AQnHcNeKtQ3mGG1Oh6oi0DDQkhKjGze8tyMj2uUx6Y8TlFeh2NxRsUEgSUtx0gZmWq4&_nc_ht=scontent.fatf4-1.fna&oh=00_AfCLy9bbOWaWeDelxBtvQ2N6zTkWsjrpI_7_xmkjwuOHTQ&oe=64C3329E" class="card-img-top" alt="...">
  		  <div class="card-body">
  				<h3 class="text-center">COMODIDAD</h3>
  		    <p class="card-text">"El Bochas Sport Club ofrece una infraestructura adecuada para cualquier tipo de actividad deportiva, recreativa y cultural.
            Si necesitás un espacio para un curso, taller o disciplina, comunicate con la Secretaría, de lunes a viernes de 16 a 20."</p>
  		  </div>
  		</div>
  		</div>
      <div class="col-md-4">
        <div class="card" style="width: 18rem;">
        <img src="https://scontent.fatf4-1.fna.fbcdn.net/v/t1.6435-9/90441301_2784161544994288_5343098642330812416_n.png?stp=dst-png_s600x600&_nc_cat=106&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeGyhMOqA6-RPVy_voCHfPiuZSeaOG2a-EVlJ5o4bZr4RSQK7uH6sveH1wpU7Shd_sBEUU5ehIC-ii3AH5R24R_E&_nc_ohc=Xr9HjcswOUQAX8p81g2&_nc_ht=scontent.fatf4-1.fna&oh=00_AfCJs-OYyd-GvY4wv3FHqk0kaFtlDGLO0vXUSxV9Knpn0A&oe=64C30AB0" class="card-img-top" alt="...">
        <div class="card-body">
          <h3 class="text-center">Entrenamientos</h3>
          <p class="card-text">"¿Extrañás jugar al fútbol? ¿Tirar al aro? ¿Patinar en un gran salón? ¿Hacer gimnasia con las profes?
              Todos queremos volver a los entrenamientos lo más pronto posible. Para eso, seamos responsables ahora.
              Es un esfuerzo individual para el bien de todos.
              Por favor, #QuedateEnCasa.."</p>
        </div>
      </div>
      </div>

  			<!-- cierre de row -->
  		</div>
		<!-- cierre de container -->
	</div>
<br>
<br>
