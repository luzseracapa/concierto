<?php
class Vacantes extends CI_Controller{
    function __construct()
    {
        parent::__construct();

        // cargamos el modelo
        $this->load->model('Vacante');
    }

    public function nuevaVacante(){
        $this->load->view('header');
        $this->load->view('vacantes/nuevaVacante');
        $this->load->view('footer');
    }

    // creamos la funcion para guardar los datos
    public function guardar(){
        $datosNuevaVacante=array(
            "puesto_vac"=>$this->input->post('puesto_vac'),
            "salario_vac"=>$this->input->post('salario_vac'),
            "horas_vac"=>$this->input->post('horas_vac'),
            "experiencia_vac"=>$this->input->post('experiencia_vac'),
            "estudios_vac"=>$this->input->post('estudios_vac'),
            "area_vac"=>$this->input->post('area_vac')
        );
        if ($this->Vacante->insertar($datosNuevaVacante)) {
            $this->session->set_flashdata('confirmacion','La vacante ha guardado exitosamente');

        } else {
            $this->session->set_flashdata('error','No se pudo guardar la vacante, intente otra vez');
        }
        redirect('vacantes/listarVacante');
    }

    public function listarVacante(){
        $data['vacantes']=$this->Vacante->obtenerTodosVacante();

        $this->load->view('header');
        $this->load->view('vacantes/listarVacante',$data);
        $this->load->view('footer');
    }

    // creamos la funcion para borrar
    public function eliminar($id_vac){
        if ($this->Vacante->borrar($id_vac)) {
            $this->session->set_flashdata('error','La vacante se ha eliminado exitosamente');

        } else {
            $this->session->set_flashdata('error','Error al eliminar la vacante, intente otra vez');
        }
        redirect('vacantes/listarVacante');
    }

    // funcion para editaar vacantes
    public function editar($id_vac){
        $data['vacanteEditar']=$this->Vacante->obtenerPorId($id_vac);
        $this->load->view('header');
        $this->load->view('vacantes/editarVacante',$data);
        $this->load->view('footer');
    }

    // proceso para actualizar datos de vacantes
    public function procesarActualizacion(){
        $datosEditados=array(
          "puesto_vac"=>$this->input->post('puesto_vac'),
          "salario_vac"=>$this->input->post('salario_vac'),
          "horas_vac"=>$this->input->post('horas_vac'),
          "experiencia_vac"=>$this->input->post('experiencia_vac'),
          "estudios_vac"=>$this->input->post('estudios_vac'),
          "area_vac"=>$this->input->post('area_vac')
        );

        $id_vac=$this->input->post('id_vac');
        if ($this->Vacante->actualizar($id_vac,$datosEditados)) {
            $this->session->set_flashdata('editar','la vacante se ha editado exitosamente');

        } else {
            $this->session->set_flashdata('editar','Error al actualizar la vacante, intente otra vez');
        }
        redirect('vacantes/listarVacante');

    }





} //cierre clase principal
?>
