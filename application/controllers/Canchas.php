<?php
class Canchas extends CI_Controller{
    function __construct()
    {
        parent::__construct();

        // // cargamos el modelo
        $this->load->model('Cancha');
    }

    public function nuevaCancha(){
        $this->load->view('header');
        $this->load->view('canchas/nuevaCancha');
        $this->load->view('footer');
    }

    // creamos la funcion para guardar los datos
    public function guardar(){
        $datosNuevaCancha=array(
            "nombre_can"=>$this->input->post('nombre_can'),
            "pais_can"=>$this->input->post('pais_can'),
            "telefono_can"=>$this->input->post('telefono_can'),
            "superficie_can"=>$this->input->post('superficie_can'),
            "aforo_can"=>$this->input->post('aforo_can'),
            "latitud_can"=>$this->input->post('latitud_can'),
            "longitud_can"=>$this->input->post('longitud_can')
        );
        if ($this->Cancha->insertar($datosNuevaCancha)) {
          // $this->session->set_flashdata("confirmacion","Cancha guardada exitosamente.");
          $this->session->set_flashdata('editar','la cancha se ha guardado exitosamente');
        } else {
          // $this->session->set_flashdata("error","Error al guardar Cancha.");
          $this->session->set_flashdata('error','Error no se ha podido guardar la cancha.');
        }
          redirect('canchas/listarCancha');
    }


    // llamdo de funcion listar canchas
    public function listarCancha(){
        $data['canchas']=$this->Cancha->obtenerTodosCancha();
        $this->load->view('header');
        $this->load->view('canchas/listarCancha', $data);
        $this->load->view('footer');
    }


// Llamado de funcion eliminar


        public function eliminar($id_can){
            if ($this->Cancha->borrar($id_can)) {
              // $this->session->set_flashdata("confirmacion","Cancha eliminada exitosamente.");
              $this->session->set_flashdata('editar','La cancha se ha eliminado exitosamente');
            } else {
              // $this->session->set_flashdata("error","Error al guardar cancha");
              $this->session->set_flashdata('error','Error no se ha podido eliminar la cancha.');
            }
            redirect('canchas/listarCancha');
        }

        ////////////renderiza vista eliminar canchas

        public function editarCancha($id_can){
            $data['canchaEditar']=$this->Cancha->obtenerPorId($id_can);
            $data['latitud_can'] = $data['canchaEditar']->latitud_can;
            $data['longitud_can'] = $data['canchaEditar']->longitud_can;
            $this->load->view('header');
            $this->load->view('canchas/editarCancha',$data);
            $this->load->view('footer');
        }

        //proceso para actualizar datos de canchas
        public function procesarActualizacion(){
            $datosEditados=array(
              "nombre_can"=>$this->input->post('nombre_can'),
              "pais_can"=>$this->input->post('pais_can'),
              "telefono_can"=>$this->input->post('telefono_can'),
              "superficie_can"=>$this->input->post('superficie_can'),
              "aforo_can"=>$this->input->post('aforo_can'),
              "latitud_can"=>$this->input->post('latitud_can'),
              "longitud_can"=>$this->input->post('longitud_can')
            );

            $id_can=$this->input->post('id_can');
            if ($this->Cancha->actualizar($id_can,$datosEditados)) {
              // $this->session->set_flashdata("confirmacion","Cancha editada exitosamente.");

              $this->session->set_flashdata('editar','La cancha se ha editado exitosamente');

            } else {
              // $this->session->set_flashdata("error","Error al editar cancha");
              $this->session->set_flashdata('error','Error no se ha podido ediatr la cancha.');

            }
            redirect('canchas/listarCancha');


        }





}


?>
