<?php
class Lugares extends CI_Controller{
    function __construct(){
        parent::__construct();

        $this->load->model('Lugar');

    }

    public function reporteEscuelas(){
        $data['escuelas']=$this->Lugar->obtenerEscuelas();
        $this->load->view('header');
        $this->load->view('lugares/reporteEscuelas', $data);
        $this->load->view('footer');
    }

///////////////////////////////reporte canchas
public function reporteCanchas(){
    $data['cancha']=$this->Lugar->obtenerCanchas();
    $this->load->view('header');
    $this->load->view('lugares/reporteCanchas', $data);
    $this->load->view('footer');
}


}
?>
