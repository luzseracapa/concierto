<?php
class Escuelas extends CI_Controller{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Escuela');
    }

    public function index(){
        $data['escuelas']=$this->Escuela->obtenerEscuelas();
        $this->load->view('header');
        $this->load->view('escuelas/index',$data);
        $this->load->view('footer');
    }

    public function nuevo(){

        $data['entrenadores']=$this->db->get('entrenador')->result();

        $this->load->view('header');
        $this->load->view('escuelas/nuevo',$data);
        $this->load->view('footer');
    }

    // funcion para guardar datos de escuela
    public function guardar(){
        $datosNuevaEscuela = array(
            "barrio_esc"=>$this->input->post('barrio_esc'),
            "ciudad_esc"=>$this->input->post('ciudad_esc'),
            "telefono_esc"=>$this->input->post('telefono_esc'),
            "latitud_esc"=>$this->input->post('latitud_esc'),
            "longitud_esc"=>$this->input->post('longitud_esc'),
            "entrenador_id"=>$this->input->post('entrenador_id'),
        );
        if ($this->Escuela->insertar($datosNuevaEscuela)) {
            $this->session->set_flashdata('confirmacion','La escuela se ha guardado exitosamente');
        } else {
            $this->session->set_flashdata('error','No se pudo guardar la escuela, intente otra vez');
        }
        redirect('escuelas/index');
    }

    // funcion para elimiar
    public function eliminar($id_esc){
        if ($this->Escuela->borrar($id_esc)) {
            $this->session->set_flashdata('error','La escuela se ha eliminado exitosamente');
            
        } else {
            $this->session->set_flashdata('error','Error al eliminar la escuela, intente otra vez');
        }
        redirect('escuelas/index');
    }

    // funcion para editar escuelas
    public function editar($id_esc){
        $data['escuelaEditar']=$this->Escuela->obtenerPorId($id_esc);
        $data['entrenadores']=$this->db->get('entrenador')->result();
        $data['valorSeleccionado'] = $data['escuelaEditar']->entrenador_id;
        $data['latitud_esc'] = $data['escuelaEditar']->latitud_esc;
        $data['longitud_esc'] = $data['escuelaEditar']->longitud_esc;

        $this->load->view('header');
        $this->load->view('escuelas/editar',$data);
        $this->load->view('footer');
    }

    // proceso para actualizacion -- capturo los datos esditados
    public function procesarActualizacion(){
        $datosEditados=array(
            "barrio_esc"=>$this->input->post('barrio_esc'),
            "ciudad_esc"=>$this->input->post('ciudad_esc'),
            "telefono_esc"=>$this->input->post('telefono_esc'),
            "latitud_esc"=>$this->input->post('latitud_esc'),
            "longitud_esc"=>$this->input->post('longitud_esc'),
            "entrenador_id"=>$this->input->post('entrenador_id'),
        );
        $id_esc=$this->input->post('id_esc');
        if ($this->Escuela->actualizar($id_esc,$datosEditados)) {
            $this->session->set_flashdata('editar','La escuela se ha editado exitosamente');
            
        } else {
            $this->session->set_flashdata('editar','Error al actualizar la escuela, intente otra vez');
        }
        redirect('escuelas/index');
        
    }


}


?>