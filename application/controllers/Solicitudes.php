<?php
class Solicitudes extends CI_Controller{
    function __construct()
    {
        parent::__construct();

        // cargamos el modelo
        $this->load->model('Solicitud');
    }



    public function nuevaSolicitud(){
      $data['vacantes']=$this->db->get('vacante')->result();
        $this->load->view('header');
        $this->load->view('solicitudes/nuevaSolicitud',$data);
        $this->load->view('footer');
    }

    // creamos la funcion para guardar los datos
    public function guardar(){
        $datosNuevaSolicitud=array(
            "nombre_sol"=>$this->input->post('nombre_sol'),
            "apellido_sol"=>$this->input->post('apellido_sol'),
            "telefono_sol"=>$this->input->post('telefono_sol'),
            "estudios_sol"=>$this->input->post('estudios_sol'),
            "salario_sol"=>$this->input->post('salario_sol'),
            "vacante_id"=>$this->input->post('vacante_id')
        );
        if ($this->Solicitud->insertar($datosNuevaSolicitud)) {
            $this->session->set_flashdata('confirmacion','El solicitud se ha guardada exitosamente');

        } else {
            $this->session->set_flashdata('error','No se pudo guardar la solicitud, intente otra vez');
        }
        redirect('solicitudes/listarSolicitud');
    }


        public function listarSolicitud(){
            $data['solicitudes']=$this->Solicitud->obtenerSolicitudes();
            $this->load->view('header');
            $this->load->view('solicitudes/listarSolicitud',$data);
            $this->load->view('footer');
        }
    // creamos la funcion para borrar
    public function eliminar($id_sol){
        if ($this->Solicitud->borrar($id_sol)) {
            $this->session->set_flashdata('error','La solicitud se ha eliminado exitosamente');

        } else {
            $this->session->set_flashdata('error','Error al eliminar la solicitud, intente otra vez');
        }
        redirect('solicitudes/listarSolicitud');
    }


    // funcion para editar vacantes
    public function editarSolicitud($id_sol){
        $data['solicitudEditar']=$this->Solicitud->obtenerPorId($id_sol);
        $data['vacantes']=$this->db->get('vacante')->result();
        $data['valorSeleccionado'] = $data['solicitudEditar']->vacante_id;

        $this->load->view('header');
        $this->load->view('solicitudes/editarSolicitud',$data);
        $this->load->view('footer');
    }

    // proceso para actualizacion -- capturo los datos esditados
    public function procesarActualizacion(){
        $datosEditados=array(
          "nombre_sol"=>$this->input->post('nombre_sol'),
          "apellido_sol"=>$this->input->post('apellido_sol'),
          "telefono_sol"=>$this->input->post('telefono_sol'),
          "estudios_sol"=>$this->input->post('estudios_sol'),
          "salario_sol"=>$this->input->post('salario_sol'),
          "vacante_id"=>$this->input->post('vacante_id'),
        );
        $id_sol=$this->input->post('id_sol');
        if ($this->Solicitud->actualizar($id_sol,$datosEditados)) {
            $this->session->set_flashdata('editar','La solicitud se ha editado exitosamente');

        } else {
            $this->session->set_flashdata('editar','Error al actualizar la solicitud, intente otra vez');
        }
        redirect('solicitudes/listarSolicitud');

    }




}


?>
