<?php

  class Lesiones extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model('Lesion');
    }
     public function index(){
       $data['lesiones']=$this->Lesion->obtenerTodos();
       $this->load->view('header');
       $this->load->view('lesiones/index',$data);
       $this->load->view('footer');

    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('lesiones/nuevo');
      $this->load->view('footer');

   }
   public function guardar(){
  $datosNuevaLesion=array(
    "Tipo_le"=>$this->input->post('Tipo_le'),
    "Descripcion_le"=>$this->input->post('Descripcion_le'),
    "Medico_le"=>$this->input->post('Medico_le'),
    "Tratamiento_le"=>$this->input->post('Tratamiento_le'),
    "Tiempo_de_recuperacion_le"=>$this->input->post('Tiempo_de_recuperacion_le'),
    "Estado_le"=>$this->input->post('Estado_le'),
    "observaciones_le"=>$this->input->post('observaciones_le')

  );

  if($this->Lesion->insertar($datosNuevaLesion)){
    redirect('lesiones/index');
  }else{
    echo "<h1>ERROR AL INSERTAR</h1>";
  }
}
//funcion para eliminar EL REGISTRO DE LESIONES
  public function eliminar($id_le){
    if ($this->Lesion->borrar($id_le)) {//invocando el modelo
      redirect('lesiones/index');
    } else {
      echo "ERROR AL BORRAR ;) ";
    }

  }
  ////////////////////////renderizar vista editar instructores
  public function editar($id_le){
    $data ["lesionEditar"]= $this->Lesion->obtenerPorId($id_le);
    $this->load-> view('header');
    $this->load-> view('lesiones/editar',$data);
    $this->load-> view('footer');

  }

  ///// PROCESO DE ACTUALIZACION
  public function procesarActualizacion(){
    $datosEditados=array(
      "Tipo_le"=>$this->input->post('Tipo_le'),
      "Descripcion_le"=>$this->input->post('Descripcion_le'),
      "Medico_le"=>$this->input->post('Medico_le'),
      "Tratamiento_le"=>$this->input->post('Tratamiento_le'),
      "Tiempo_de_recuperacion_le"=>$this->input->post('Tiempo_de_recuperacion_le'),
      "Estado_le"=>$this->input->post('Estado_le'),
      "observaciones_le"=>$this->input->post('observaciones_le')
    );
    $id_le=$this->input->post("id_le");
    if ($this->Lesion->actualizar($id_le,$datosEditados)) {
      redirect("lesiones/index");
    } else {
      echo "ERROR AL ACTUALIZAR";
    }

  } ////fin funcion procesar actualizacion
}//CIERRE DE LA CLASE

 ?>
