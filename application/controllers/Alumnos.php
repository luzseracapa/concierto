<?php

class Alumnos extends CI_Controller {

    function __construct() {
        parent::__construct();

        // llamamos al modelo
        $this->load->model('Alumno');


    }

    public function index() {
        $data['alumnos'] = $this->Alumno->obtenerAlumnosEscuelas();
        $this->load->view('header');
        $this->load->view('alumnos/index', $data);
        $this->load->view('footer');
    }

    public function nuevo() {
        $data['escuelas'] = $this->db->get('escuela')->result();

        $this->load->view('header');
        $this->load->view('alumnos/nuevo', $data);
        $this->load->view('footer');
    }

    // creamos la funcion para guardar los datos del alumno
    public function guardar(){
        $datosNuevoAlumno = array(
            "cedula_alu" => $this->input->post('cedula_alu'),
            "nombre_alu" => $this->input->post('nombre_alu'),
            "apellido_alu" => $this->input->post('apellido_alu'),
            "fechaNac_alu" => $this->input->post('fechaNac_alu'),
            "genero_alu" => $this->input->post('genero_alu'),
            "id_escuela" => $this->input->post('id_escuela'),
            "estado_alu" => $this->input->post('estado_alu')
        );

        if ($this->Alumno->insertar($datosNuevoAlumno)) {
            $this->session->set_flashdata('confirmacion','El alumno se ha guardado exitosamente');

        } else {
            $this->session->set_flashdata('error','No se pudo guardar el alumno, intente otra vez');
        }
        redirect('alumnos/index');
    }

    // funcion eliminar
    public function eliminar($id_alu){
        if ($this->Alumno->borrar($id_alu)) {
          $this->session->set_flashdata('error','El alumno se ha eliminado exitosamente');
            
        } else {
            $this->session->set_flashdata('error','Error al eliminar el alumno, intente otra vez');
        }
        redirect('alumnos/index');
    }


    //funcion para editar alumnos
    public function editar($id_alu){
        $data['escuelaEditar'] = $this->Alumno->obtenerPorID($id_alu);
        $data['escuelas'] = $this->db->get('escuela')->result();

        $data['valorSeleccionado'] = $data['escuelaEditar']->id_escuela;
        $data['genero_alu'] = $data['escuelaEditar']->genero_alu;

        // Obtén el estado actual del alumno
        $data['estado_alu'] = $data['escuelaEditar']->estado_alu; //lo que hago es que le asigno el valor del estado del alumno a la variable $data['estado_alu']


        $this->load->view('header');
        $this->load->view('alumnos/editar', $data);
        $this->load->view('footer');

    }



    // funcion editar
    public function procesarActualizacion(){
        $datosEditados=array(
            "cedula_alu" => $this->input->post('cedula_alu'),
            "nombre_alu" => $this->input->post('nombre_alu'),
            "apellido_alu" => $this->input->post('apellido_alu'),
            "fechaNac_alu" => $this->input->post('fechaNac_alu'),
            "genero_alu" => $this->input->post('genero_alu'),
            "id_escuela" => $this->input->post('id_escuela'),   
            "estado_alu" => $this->input->post('estado_alu')
        );
        $id_alu = $this->input->post('id_alu');
        if ($this->Alumno->actualizar($id_alu, $datosEditados)) {
            $this->session->set_flashdata('editar','El alumno se ha editado exitosamente');

        } else {
            $this->session->set_flashdata('editar','Error al actualizar el alumno, intente otra vez');
        }
        redirect('alumnos/index');
    }


}//cierre de la clase

?>
