<?php
class Entrenadores extends CI_Controller{
    function __construct()
    {
        parent::__construct();

        // cargamos el modelo
        $this->load->model('Entrenador');
    }

    public function index(){
        $data['entrenadores']=$this->Entrenador->obtenerTodos();

        $this->load->view('header');
        $this->load->view('entrenadores/index',$data);
        $this->load->view('footer');
    }

    public function nuevo(){
        $this->load->view('header');
        $this->load->view('entrenadores/nuevo');
        $this->load->view('footer');
    }

    // creamos la funcion para guardar los datos
    public function guardar(){
        $datosNuevoEntrenador=array(
            "cedula_ent"=>$this->input->post('cedula_ent'),
            "nombre_ent"=>$this->input->post('nombre_ent'),
            "apellido_ent"=>$this->input->post('apellido_ent'),
            "direccion_ent"=>$this->input->post('direccion_ent'),
            "email_ent"=>$this->input->post('email_ent'),
            "telefono_ent"=>$this->input->post('telefono_ent')
        );
        if ($this->Entrenador->insertar($datosNuevoEntrenador)) {
            $this->session->set_flashdata('confirmacion','El entrenador se ha guardado exitosamente');
            
        } else {
            $this->session->set_flashdata('error','No se pudo guardar el entrenador, intente otra vez');
        }
        redirect('entrenadores/index');
    }

    // creamos la funcion para borrar
    public function eliminar($id_ent){
        if ($this->Entrenador->borrar($id_ent)) {
            $this->session->set_flashdata('error','El entrenador se ha eliminado exitosamente');
            
        } else {
            $this->session->set_flashdata('error','Error al eliminar el entrenador, intente otra vez');
        }
        redirect('entrenadores/index');
    }

    // funcion para editaar instructores
    public function editar($id_ent){
        $data['entrenadorEditar']=$this->Entrenador->obtenerPorId($id_ent);
        $this->load->view('header');
        $this->load->view('entrenadores/editar',$data);
        $this->load->view('footer');
    }

    //proceso para actualizar datos de instructores
    public function procesarActualizacion(){
        $datosEditados=array(
            "cedula_ent"=>$this->input->post('cedula_ent'),
            "nombre_ent"=>$this->input->post('nombre_ent'),
            "apellido_ent"=>$this->input->post('apellido_ent'),
            "direccion_ent"=>$this->input->post('direccion_ent'),
            "email_ent"=>$this->input->post('email_ent'),
            "telefono_ent"=>$this->input->post('telefono_ent')
        );

        $id_ent=$this->input->post('id_ent');
        if ($this->Entrenador->actualizar($id_ent,$datosEditados)) {
            $this->session->set_flashdata('editar','El entrenador se ha editado exitosamente');
            
        } else {
            $this->session->set_flashdata('editar','Error al actualizar el entrenador, intente otra vez');
        }
        redirect('entrenadores/index');
        
    }





}


?>