<?php
class Login extends CI_Controller{
    public function __construct(){
        parent::__construct();

    }

    public function index(){
        // Validamos si existe la sesión
        if($this->session->userdata('email_us')){
            redirect('welcome');
        }

        if(isset($_POST['email_us'])){
            $this->load->model('loginModel');
            if($this->loginModel->login($_POST['email_us'],$_POST['password_us'])){
                $this->session->set_userdata('email_us',$_POST['email_us']);

                $this->session->set_flashdata("bienvenida","Bienvenido al sistema"." "."ADMINISTRADOR");
                redirect('alumnos/index');
            }else{
                redirect('login#bad-password');
            }
        }

        $this->load->view('header');
        $this->load->view('login');
        $this->load->view('footer');
    }

    // Función para cerrar sesión
    public function logout(){
        $this->session->sess_destroy();
        redirect('welcome');
    }
}
?>
