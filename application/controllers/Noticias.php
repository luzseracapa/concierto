<?php
class Noticias extends CI_Controller{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Noticia');
    }

    public function index(){
        $data['noticias']=$this->Noticia->obtenerNoticias();
         // $data['noticias'] = $noticias;
        $this->load->view('header');
        $this->load->view('noticias/index',$data);
        $this->load->view('footer');
    }

    public function nuevo(){
        $data['usuarios']=$this->db->get('usuario')->result();
        $this->load->view('header');
        $this->load->view('noticias/nuevo',$data);
        $this->load->view('footer');
    }

    // funcion para guardar datos de escuela
    public function guardar(){
        $datosNuevaNoticia= array(
            "titulo"=>$this->input->post('titulo'),
            "contenido"=>$this->input->post('contenido'),
            "fecha_publicacion"=>$this->input->post('fecha_publicacion'),
            "categoria"=>$this->input->post('categoria'),
            "estado"=>$this->input->post('estado'),
            "id_us"=>$this->input->post('id_us'),
        );
        if ($this->Noticia->insertar($datosNuevaNoticia)) {
            $this->session->set_flashdata('confirmacion','La noticia se ha guardado exitosamente');
        } else {
            $this->session->set_flashdata('error','No se pudo guardar la noticia, intente otra vez');
        }
        redirect('noticias/index');
    }

    // // funcion para elimiar
    public function eliminar($id_noticia){
        if ($this->Noticia->borrar($id_noticia)) {
            $this->session->set_flashdata('error','La noticia se ha eliminado exitosamente');

        } else {
            $this->session->set_flashdata('error','Error al eliminar la noticia, intente otra vez');
        }
        redirect('noticias/index');
    }

    // funcion para editar escuelas
    public function editar($id_noticia){
        $data['noticiaEditar']=$this->Noticia->obtenerPorId($id_noticia);
        $data['usuarios']=$this->db->get('usuario')->result();

        $data['valorSeleccionado'] = $data['noticiaEditar']->id_us;
        $data['estado'] = $data['noticiaEditar']->estado;

        $data['categoria'] = $data['noticiaEditar']->categoria;


        $this->load->view('header');
        $this->load->view('noticias/editar',$data);
        $this->load->view('footer');
    }

    // proceso para actualizacion -- capturo los datos esditados
    public function procesarActualizacion(){
        $datosEditados=array(
          "titulo"=>$this->input->post('titulo'),
          "contenido"=>$this->input->post('contenido'),
          "fecha_publicacion"=>$this->input->post('fecha_publicacion'),
          "categoria"=>$this->input->post('categoria'),
          "estado"=>$this->input->post('estado'),
          "id_us"=>$this->input->post('id_us'),
        );
        $id_noticia=$this->input->post('id_noticia');
        if ($this->Noticia->actualizar($id_noticia,$datosEditados)) {
            $this->session->set_flashdata('editar','La noticia se ha editado exitosamente');

        } else {
            $this->session->set_flashdata('editar','Error al actualizar la noticia, intente otra vez');
        }
        redirect('noticias/index');

    }


}


?>
