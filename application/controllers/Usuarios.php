<?php
class Usuarios extends CI_Controller{
    function __construct()
    {
        parent::__construct();

        // cargamos el modelo
        $this->load->model('Usuario');
    }

    public function index(){
        $data['usuarios']=$this->Usuario->obtenerTodos();

        $this->load->view('header');
        $this->load->view('usuarios/index',$data);
        $this->load->view('footer');
    }

    public function nuevo(){
        $this->load->view('header');
        $this->load->view('usuarios/nuevo');
        $this->load->view('footer');
    }

    // creamos la funcion para guardar los datos
    public function guardar(){
        $datosNuevoUsuario=array(
            "cedula_us"=>$this->input->post('cedula_us'),
            "nombre_us"=>$this->input->post('nombre_us'),
            "apellido_us"=>$this->input->post('apellido_us'),
            "direccion_us"=>$this->input->post('direccion_us'),
            "email_us"=>$this->input->post('email_us'),
            "password_us"=>$this->input->post('password_us'),
            "perfil_us"=>$this->input->post('perfil_us'),
              "genero_us"=>$this->input->post('genero_us'),
            "telefono_us"=>$this->input->post('telefono_us'),
            "fechaNac_us"=>$this->input->post('fechaNac_us')
        );
        if ($this->Usuario->insertar($datosNuevoUsuario)) {
            $this->session->set_flashdata('confirmacion','El usuario se ha guardado exitosamente');

        } else {
            $this->session->set_flashdata('error','No se pudo guardar el usuario, intente otra vez');
        }
        redirect('usuarios/index');
    }

    // // creamos la funcion para borrar
    public function eliminar($id_us){
        if ($this->Usuario->borrar($id_us)) {
            $this->session->set_flashdata('error','El usuario se ha eliminado exitosamente');

        } else {
            $this->session->set_flashdata('error','Error al eliminar el usuario, intente otra vez');
        }
        redirect('usuarios/index');
    }
    //
    // // funcion para editaar instructores
    public function editar($id_us){
        $data['usuarioEditar']=$this->Usuario->obtenerPorId($id_us);


        $this->load->view('header');
        $this->load->view('usuarios/editar',$data);
        $this->load->view('footer');
    }

    //proceso para actualizar datos de instructores
    public function procesarActualizacion(){
        $datosEditados=array(
          "cedula_us"=>$this->input->post('cedula_us'),
          "nombre_us"=>$this->input->post('nombre_us'),
          "apellido_us"=>$this->input->post('apellido_us'),
          "direccion_us"=>$this->input->post('direccion_us'),
          "email_us"=>$this->input->post('email_us'),
          "password_us"=>$this->input->post('password_us'),
            "perfil_us"=>$this->input->post('perfil_us'),
            "genero_us"=>$this->input->post('genero_us'),
          "telefono_us"=>$this->input->post('telefono_us'),
          "fechaNac_us"=>$this->input->post('fechaNac_us')
        );

        $id_us=$this->input->post('id_us');
        if ($this->Usuario->actualizar($id_us,$datosEditados)) {
            $this->session->set_flashdata('editar','El usuario se ha editado exitosamente');

        } else {
            $this->session->set_flashdata('editar','Error al actualizar el usuario, intente otra vez');
        }
        redirect('usuarios/index');

    }
}
?>
