<?php

class Entrenador extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    // funcion para insertar datos
    function insertar($datos){
        return $this->db->insert('entrenador', $datos);

    }

    // funcion par obtener todos los entrenadores
    function obtenerTodos(){
        $listadoEntrenadores=$this->db->get('entrenador');
        if ($listadoEntrenadores->num_rows()>0) {
            return $listadoEntrenadores->result();
        } else {
            return false;
        }
    }

    // funcion para borrar
    function borrar($id_ent){
        $this->db->where('id_ent', $id_ent);
        if ($this->db->delete('entrenador')) {
            return true;
        } else {
            return false;
        }  
    }
    // funcion para consulatar un isntructor
    function obtenerPorId($id_ent){
        $this->db->where('id_ent', $id_ent);
        $entrenador=$this->db->get('entrenador');
        if ($entrenador->num_rows()>0) {
            return $entrenador->row();
        } else {
            return false;
        }

    }

    // funcion para actualizar datos entrenador
    function actualizar($id_ent,$datos){
        $this->db->where('id_ent', $id_ent);
        return $this->db->update('entrenador', $datos);

    }
}  // fin de la clase


?>