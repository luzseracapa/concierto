<?php

class Vacante extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    // funcion para insertar datos
    function insertar($datos){
        return $this->db->insert('vacante', $datos);

    }

    // funcion par obtener todos los entrenadores
    function obtenerTodosVacante(){
        $listadoVacantes=$this->db->get('vacante');
        if ($listadoVacantes->num_rows()>0) {
            return $listadoVacantes->result();
        } else {
            return false;
        }
    }

    // funcion para borrar
    function borrar($id_vac){
        $this->db->where('id_vac', $id_vac);
        if ($this->db->delete('vacante')) {
            return true;
        } else {
            return false;
        }
    }
    // funcion para consulatar un vacante
    function obtenerPorId($id_vac){
        $this->db->where('id_vac', $id_vac);
        $vacante=$this->db->get('vacante');
        if ($vacante->num_rows()>0) {
            return $vacante->row();
        } else {
            return false;
        }

    }

    // funcion para actualizar datos entrenador
    function actualizar($id_vac,$datos){
        $this->db->where('id_vac', $id_vac);
        return $this->db->update('vacante', $datos);

    }
}  // fin de la clase
?>
