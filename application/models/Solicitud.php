<?php

class Solicitud extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    // funcion insertar datos de escuela
    function insertar($datos){
        return $this->db->insert('solicitud',$datos);
    }

    //funcion para obtener datos de escuela
    function obtenerTodos(){
        $listadoSolicitudes=$this->db->get('solicitud');
        if ($listadoSolicitudes->num_rows()>0) {
            return $listadoSolicitudes->result();
        } else {
            return false;
        }

    }

    // crear la funciona para obtenre datos con join del entrenador
    function obtenerSolicitudes(){
        $this->db->select('solicitud.*, vacante.puesto_vac');
        $this->db->from('solicitud');
        $this->db->join('vacante', 'vacante.id_vac = solicitud.vacante_id');
        return $this->db->get()->result();
    }
    // funcion para consultar escuela por id
    function obtenerPorId($id_sol){
        $this->db->where('id_sol',$id_sol);
        $solicitud=$this->db->get('solicitud');
        if ($solicitud->num_rows()>0) {
            return $solicitud->row();
        }
        return false;

    }

    // funcion para borrar datos de escuela
    function borrar($id_sol){
        $this->db->where('id_sol',$id_sol);
        if ($this->db->delete('solicitud')) {
            return true;
        } else {
            return false;
        }
    }



    // funcion para actualizar un registro de escuela
    function actualizar($id_sol, $datos){
        $this->db->where('id_sol',$id_sol);
        return $this->db->update('solicitud',$datos);
    }
}

?>
