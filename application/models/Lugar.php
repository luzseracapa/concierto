<?php
class Lugar extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function obtenerEscuelas(){
        $listadoEscuelas=$this->db->get('escuela');
        if ($listadoEscuelas->num_rows()>0) {
            return $listadoEscuelas->result();
        }
        return false;
    }



    //creamos la funcion para obtener las canchas
    function obtenerCanchas(){
        $listadoCanchas=$this->db->get('cancha'); //obtenemos los registros de la tabla canchas

        if ($listadoCanchas->num_rows()>0) {
            return $listadoCanchas->result();
        }
        return false;
    }



}
?>
