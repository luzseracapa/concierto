<?php

class Usuario extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    // funcion para insertar datos
    function insertar($datos){
        return $this->db->insert('usuario', $datos);

    }

    // funcion par obtener todos los usuario
    function obtenerTodos(){
        $listadoUsuarios=$this->db->get('usuario');
        if ($listadoUsuarios->num_rows()>0) {
            return $listadoUsuarios->result();
        } else {
            return false;
        }
    }

    // // funcion para borrar
    function borrar($id_us){
        $this->db->where('id_us', $id_us);
        if ($this->db->delete('usuario')) {
            return true;
        } else {
            return false;
        }
    }
    // funcion para consulatar un usuario
    function obtenerPorId($id_us){
        $this->db->where('id_us', $id_us);
        $usuario=$this->db->get('usuario');
        if ($usuario->num_rows()>0) {
            return $usuario->row();
        } else {
            return false;
        }

    }

    // funcion para actualizar datos usuario
    function actualizar($id_us,$datos){
        $this->db->where('id_us', $id_us);
        return $this->db->update('usuario', $datos);

    }
  
}  // fin de la clase


?>
