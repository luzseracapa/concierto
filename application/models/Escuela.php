<?php

class Escuela extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    // funcion insertar datos de escuela
    function insertar($datos){
        return $this->db->insert('escuela',$datos);
    }

    //funcion para obtener datos de escuela
    function obtnerTodos(){
        $listadoEscuelas=$this->db->get('escuela');
        if ($listadoEscuelas->num_rows()>0) {
            return $listadoEscuelas->result();
        } else {
            return false;
        }

    }

    // crear la funciona para obtenre datos con join del entrenador
    function obtenerEscuelas(){
        $this->db->select('escuela.*, entrenador.nombre_ent, entrenador.apellido_ent');
        $this->db->from('escuela');
        $this->db->join('entrenador', 'entrenador.id_ent = escuela.entrenador_id');
        return $this->db->get()->result();
    }

    // funcion para borrar datos de escuela
    function borrar($id_esc){
        $this->db->where('id_esc',$id_esc);
        if ($this->db->delete('escuela')) {
            return true;
        } else {
            return false;
        }
    }

    // funcion para consultar escuela por id
    function obtenerPorId($id_esc){
        $this->db->where('id_esc',$id_esc);
        $escuela=$this->db->get('escuela');
        if ($escuela->num_rows()>0) {
            return $escuela->row();
        }
        return false;

    }

    // funcion para actualizar un registro de escuela
    function actualizar($id_esc, $datos){
        $this->db->where('id_esc',$id_esc);
        return $this->db->update('escuela',$datos);
    }
}

?>
