
<?php

class Noticia extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    // funcion insertar datos de escuela
    function insertar($datos){
        return $this->db->insert('noticia',$datos);
    }

    //funcion para obtener datos de escuela
    function obtnerTodos(){
        $listadoNoticias=$this->db->get('noticia');
        if ($listadoNoticias->num_rows()>0) {
            return $listadoNoticias->result();
        } else {
            return false;
        }

    }

    // crear la funciona para obtenre datos con join del entrenador
    function obtenerNoticias(){
        $this->db->select('noticia.*, usuario.nombre_us, usuario.apellido_us');
        $this->db->from('noticia');
        $this->db->join('usuario', 'usuario.id_us = noticia.id_us');
        return $this->db->get()->result();
    }

    //
    // funcion para borrar datos de escuela
    function borrar($id_noticia){
        $this->db->where('id_noticia',$id_noticia);
        if ($this->db->delete('noticia')) {
            return true;
        } else {
            return false;
        }
    }
    //
    // funcion para consultar escuela por id
    function obtenerPorId($id_noticia){
        $this->db->where('id_noticia',$id_noticia);
        $noticia=$this->db->get('noticia');
        if ($noticia->num_rows()>0) {
            return $noticia->row();
        }
        return false;

    }

    // funcion para actualizar un registro de escuela
    function actualizar($id_noticia, $datos){
        $this->db->where('id_noticia',$id_noticia);
        return $this->db->update('noticia',$datos);
    }
}

?>
