<?php

class Alumno extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    // funcion para insertar datos de alumno
    function insertar($datos){
        return $this->db->insert('alumno', $datos);
        
    }

    // funcion para obtener los datos de la tabla alumno
    public function obtenerTodos(){
        $listadoAlumnos=$this->db->get('alumno');
        if ($listadoAlumnos->num_rows()>0) {
            return $listadoAlumnos->result();
        } else {
            return false;
        }
    }

    // funcion para elimimar
    function borrar($id_alu){
        $this->db->where('id_alu', $id_alu);
        if ($this->db->delete('alumno')) {
            return true;
        } else {
            return false;
        }
        
    }

    // funcion para obtener los datos de la tabla alumno con join a escuela
    function obtenerAlumnosEscuelas(){
        $this->db->select('alumno.id_alu, alumno.cedula_alu, alumno.nombre_alu, alumno.apellido_alu, alumno.fechaNac_alu, alumno.genero_alu, escuela.ciudad_esc, alumno.estado_alu');
        $this->db->from('alumno');
        $this->db->join('escuela', 'escuela.id_esc = alumno.id_escuela');
        $query = $this->db->get();
        return $query->result();
    }

    // funcion para obtener el id de la tabla alumno
    function obtenerPorID($id_alu){
        $this->db->where('id_alu', $id_alu);

        $alumno=$this->db->get('alumno');
        if($alumno->num_rows()>0){
            return $alumno->row();
        }
        return false;
    }

    // funcion para actualizar el registro
    function actualizar($id_alu, $datos){
        $this->db->where('id_alu', $id_alu);
        return $this->db->update('alumno', $datos);
    }



}//cierre de la clase


?>