<?php
  /**
   *
   */
  class Lesion extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }
    //funcion para insertar un instructot
    function insertar($datos){
      //ACTIVE_RECORD > en CodeIgniter
      return $this->db->insert("lesion",$datos);
      //Inseccion sQL
    }
    //FUNCION PARA CONSULTAR INSTRUCTORES LINEAS DE CODIGOS NUEVOS
    function obtenerTodos(){
      $listadoLesiones=$this->db->get("lesion");
      //VALIDAR PARA QUE NO DE ERRORES
      //SIEMPRE VALIDAR CON UN IF PARA QUE NO HAYA ERRORES
      if($listadoLesiones->num_rows()>0) { // SI HAY DATOS
        return $listadoLesiones->result();
      }else { // NO HAY DATOS
        return false;
      }
    }
    function borrar($id_le){
      //delete from instructor where id_ins=
      $this->db->where("id_le", $id_le);
      return $this->db->delete("lesion");
    }
    //Funcion para consultar un intructor especifico
    function obtenerPorId($id_le){
      $this->db->where("id_le",$id_le);
      $lesion=$this->db->get("lesion");
      if ($lesion->num_rows()>0) {
        return $lesion->row();
      }
      return false;
    }
    //funcion para actualizar un instructores
    function actualizar($id_le,$datos){
      $this->db->where("id_le",$id_le);
        return $this->db->update('lesion',$datos);
      }
  }//Cierre de la clase
